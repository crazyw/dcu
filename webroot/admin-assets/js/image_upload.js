(function (factory) {
    'use strict';
    if (typeof define === 'function' && define.amd) {
        // AMD is used - Register as an anonymous module.
        define(['jquery', 'moment'], factory);
    } else if (typeof exports === 'object') {
        factory(require('jquery'), require('moment'));
    } else {
        // Neither AMD nor CommonJS used. Use global variables.
        if (typeof jQuery === 'undefined') {
            throw 'bootstrap-datetimepicker requires jQuery to be loaded first';
        }
        if (typeof moment === 'undefined') {
            throw 'bootstrap-datetimepicker requires Moment.js to be loaded first';
        }
        factory(jQuery, moment);
    }
}(function ($) {

    var imageUpload = function (element, options) {

        var button,
            fileInput,
            crop,
            notify,
            unique

        /********************************************************************************
         *
         * Private functions
         *
         ********************************************************************************/
        setTemplate = function (name) {
            template = _.template($('#'+name).html());
            element.html(template(options.template_options));
        },
        previewImage = function (data) {
            options.template_options.image = data.path+'/'+data.filename;
            options.template_options.filename = data.filename;
            setTemplate(options.template_image);
            reset();
        },
        getUniqueId = function(prefix, more_entropy) {
            if (typeof prefix === 'undefined') {
                prefix = "";
            }

            var retId;
            var formatSeed = function (seed, reqWidth) {
                seed = parseInt(seed, 10).toString(16); // to hex str
                if (reqWidth < seed.length) { // so long we split
                    return seed.slice(seed.length - reqWidth);
                }
                if (reqWidth > seed.length) { // so short we pad
                    return Array(1 + (reqWidth - seed.length)).join('0') + seed;
                }
                return seed;
            };

            // BEGIN REDUNDANT
            if (!this.php_js) {
                this.php_js = {};
            }
            // END REDUNDANT
            if (!this.php_js.uniqidSeed) { // init seed with big random int
                this.php_js.uniqidSeed = Math.floor(Math.random() * 0x75bcd15);
            }
            this.php_js.uniqidSeed++;

            retId = prefix; // start with prefix, add current milliseconds hex string
            retId += formatSeed(parseInt(new Date().getTime() / 1000, 10), 8);
            retId += formatSeed(this.php_js.uniqidSeed, 5); // add seed hex string
            if (more_entropy) {
                // for more entropy we add a float lower to 10
                retId += (Math.random() * 10).toFixed(8).toString();
            }

            return retId;
        },
        addUniqueId = function () {
            element.append('<input type="hidden" class="random_unique" name="random['+options.template_options.name+']" value="'+getUniqueId() +'">');
            unique = $('.random_unique');
        },
        /**
         * NOTIFY
         */
        notify = {
            loaderStart: function () {

            },
            loaderEnd: function () {

            }
        },
        /**
         * CROP
         */
        crop = {
            cropper : false,
            open: function (data) {
                if(data.crop){
                    crop.setImage(data);
                    modal.open();
                    imageCropper = document.getElementById(options.crop.image);
                    crop.cropper = new Cropper(imageCropper, {
                        aspectRatio: data.aspectRatio,
                        minContainerWidth : 568,
                        minContainerHeight: 400,
                        crop: function(e) {
                        }
                    });
                    $('#'+options.crop.form).append(unique);
                }else{
                    previewImage(data);
                }
            },
            setImage: function (data) {
                $('#'+options.crop.image).attr('src',data.path+'/'+data.filename);
            }
        },
        /**
         * MODAL
         */
        modal = {
            open: function () {
                $(options.modal).modal('show');
            },
            clear: function () {
                $(options.crop.image).attr('src','');
            }
        },
        /**
         * AJAX
         */
        ajax = {
            upload: function () {
                notify.loaderStart();
                addUniqueId();
                $(options.form).ajaxSubmit({
                    url: options.ajax.uploadUrl,
                    dataType: "json",
                    data:{
                        options:options.template_options
                    },
                    success: function (response) {
                        notify.loaderEnd();
                        $.each(response, function( i, v ) {
                            if(i != 'error'){
                                crop.open(v);
                            }
                        })
                    }
                });
            },
            crop: function () {
                notify.loaderStart();
                crop.cropper.getCroppedCanvas();

                crop.cropper.getCroppedCanvas({
                    width: 160,
                    height: 90
                });
                crop.cropper.getCroppedCanvas().toBlob(function (blob) {
                    var formData = new FormData(document.getElementById(options.crop.form));

                    formData.append('croppedImage', blob);
                    formData.append('options', options.template_options);
                    // Use `jQuery.ajax` method
                    $.ajax(options.ajax.cropUrl, {
                        method: "POST",
                        data: formData,
                        processData: false,
                        contentType: false,
                        dataType: "json",
                        success: function (response) {
                            previewImage();
                        },
                        error: function () {
                            console.log('Upload error');
                        }
                    });
                });
            },
            delete: function () {

            }
        },
        /**
         * EVENTS
         */
        deleteImage = function (e) {
            if(confirm('You are sure?')){
                ajax.delete();
                setTemplate(options.template);
                reset();
            }
        }
        ,
        click = function (e) {
            fileInput.trigger('click');
            // fileInput.on('click', '[data-action]', doAction)
            return false;
        },
        change = function (e) {
            ajax.upload();
            return false;
        },
        submit = function (e) {
            ajax.crop();
            return false;
        },
        reset = function () {
            attachImageElementEvents();
        },
        /**
         * INIT
         */
        attachImageElementEvents = function () {
            button.on({
                click:click
            });
            fileInput.on({
                change:change
            });
            $(options.deleteButton).on({
                click:deleteImage
            });
            $('#'+options.crop.form).on({
                submit:submit
            });
        };
        /**
         * BASE VERIFICATION
         */
        if (element.is('div')) {
            if(element.attr('data-filename')){
                previewImage({
                    filename: element.attr('data-filename'),
                    path: element.attr('data-path'),
                    image: element.attr('data-path')+'/'+element.attr('data-filename')
                })
            }else{
                setTemplate(options.template);
            }

            // setTemplate(options.template_image);

            button = element.find(options.addButton);
            fileInput = element.find(options.fileInput);
        } else {
            throw new Error('CSS class cannot be applied to non div element');
        }

        attachImageElementEvents();
    };

    /********************************************************************************
     *
     * jQuery plugin constructor and defaults object
     *
     ********************************************************************************/

    $.fn.amigo_image_upload = function (options) {
        return this.each(function () {
            var $this = $(this);
            if (!$this.data('ImageUpload')) {
                // create a private copy of the defaults object
                options = $.extend(true, {}, $.fn.amigo_image_upload.defaults, options);
                $this.data('ImageUpload', imageUpload($this, options));
            }
        });
    };

    $.fn.amigo_image_upload.defaults = {
        template : 'image',
        template_options: {},
        addButton: '.btnAddItem',
        fileInput: '.image-file',
        ajaxUploadUrl: '',
        form : 'form',
        keyBinds:{
            
        }
    };
}));