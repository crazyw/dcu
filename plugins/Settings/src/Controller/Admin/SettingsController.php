<?php
namespace Settings\Controller\Admin;

use App\Controller\Admin\SettingsController as BaseSettings;
use Cake\Filesystem\Folder;
use Cake\I18n\I18n;
use Cake\Core\Configure;
/**
 * Settings Controller
 *
 * @property \App\Model\Table\SettingsTable $Settings
 */
class SettingsController extends BaseSettings
{

	/**
	 * Index method
	 *
	 * @return \Cake\Network\Response|null
	 */
	public function index()
	{
		parent::index();
		$languageTable = \Cake\ORM\TableRegistry::get('Languages');
		if ($this->request->is('post')) {
			$options = [];
			/**
			 * Upload favicons
			 */
			if(isset($_FILES['favicon']) && $_FILES['favicon']['tmp_name'] && $_FILES['favicon']['error'] == 0){
				$favicon_upload_name ='tmp-'.$_FILES['favicon']['name'];
				$favicon_upload_path = WWW_ROOT.'img/favicons/';

				$deleteDir = new \Cake\Filesystem\Folder($favicon_upload_path);
				if(!is_dir($favicon_upload_path) || $deleteDir->delete()) {
					$createDir = new \Cake\Filesystem\Folder();
					if ($createDir->create($favicon_upload_path, 0777) && move_uploaded_file($_FILES['favicon']['tmp_name'], $favicon_upload_path . $favicon_upload_name)) {
						$source_image = $favicon_upload_path.$favicon_upload_name;
						foreach (dev_conf('Config.favicons') as $thumb) {
							$destination = $favicon_upload_path.$thumb['name'];
							if (!empty($thumb['width']) && empty($thumb['height'])) {
								$this->AmigoFile->resizeByWidth($source_image, $thumb['width'], $destination);
							} elseif (empty($thumb['width']) && !empty($thumb['height'])) {
								$this->AmigoFile->resizeByHeight($source_image, $thumb['height'], $destination);
							} elseif (!empty($thumb['width']) && !empty($thumb['height'])) {
								$this->AmigoFile->smartCrop($source_image, $thumb['width'], $thumb['height'], $destination);
							}
						}
					}
				}
			}
            /**
             * Upload home page image
             */

            if (isset($_FILES['homepic']) && $_FILES['homepic']['tmp_name'] && $_FILES['homepic']['error'] == 0) {
                $logo_upload_name = $_FILES['homepic']['name'];
                $logo_upload_path = WWW_ROOT.'img/home/';

                $deleteDir = new \Cake\Filesystem\Folder($logo_upload_path, false);
                if(!is_dir($logo_upload_path) || $deleteDir->delete()) {
                    $createDir = new \Cake\Filesystem\Folder();
                    if ($createDir->create($logo_upload_path, 0777)) {
                        move_uploaded_file($_FILES['homepic']['tmp_name'], $logo_upload_path . $logo_upload_name);
                    }
                }
            }

			/**
			 * Save setting in db
			 */
			foreach ($this->request->getData() as $main => $item) {
				if(is_array($item)){
					foreach ($item as $key => $value) {
						$options[$main][$key] = $value;
						$check = $this->Settings->findByColGroupAndColKey($main,$key)->first();
						if(!$check){
							$check = $this->Settings->newEntity();
						}
						$check->col_group = $main;
						$check->col_key = $key;
						$check->value = $value;
						$this->Settings->save($check);
					}
				}
			}

			//

            /**
             * Save setting in db
             */
            foreach ($this->request->getData() as $main => $item) {
                if(is_array($item)){
                    if ($main == 'homepic') {
                        if (isset($_FILES['homepic']) && $_FILES['homepic']['tmp_name'] && $_FILES['homepic']['error'] == 0) {
                            foreach ($item as $key => $value) {
                                $options[$main][$key] = $value;
                                $check = $this->Settings->findByColGroupAndColKey($main, $key)->first();
                                if (!$check) {
                                    $check = $this->Settings->newEntity();
                                }
                                $check->col_group = $main;
                                $check->col_key = $key;
                                $check->value = $value;
                                $this->Settings->save($check);
                            }
                        }
                    }else {
                        foreach ($item as $key => $value) {
                            $options[$main][$key] = $value;
                            $check = $this->Settings->findByColGroupAndColKey($main,$key)->first();
                            if(!$check){
                                $check = $this->Settings->newEntity();
                            }
                            $check->col_group = $main;
                            $check->col_key = $key;
                            $check->value = $value;
                            $this->Settings->save($check);
                        }
                    }
                }
            }
			/**
			 * Clear cache
			 */
			Configure::write('Settings', $options);
			Configure::store('Settings', 'default');
			\Cake\Cache\Cache::clearAll(false);

			foreach (Configure::read('Settings.Languages') as $language => $status) {
				$languageTable->query()->update()->set(['admin' => $status])->where(['code'=>$language])->execute();
			}
			foreach (Configure::read('Settings.LanguagesFront') as $language => $status) {
				$languageTable->query()->update()->set(['site' => $status])->where(['code'=>$language])->execute();
			}

			$this->Flash->set('The '.CONTROLLER_NAME.' has been updated.',
				[
					'key' => 'admin',
					'element' => 'admin',
					'params' => [
						'class' => 'success'
					]
                ]
			);
			return $this->redirect(['action' => 'index']);
		}

		$setting = Configure::read('Settings');
		$language_list = $languageTable->find('list')->all()->toArray();
		$this->set(compact('setting','language_list'));
	}

	/**
	 * deleteFavicon method
	 * @return \Cake\Network\Response|null
	 */
	public function deleteFavicon()
	{
		$this->response->type('json');

		$deleteDir = new \Cake\Filesystem\Folder(WWW_ROOT.'img/favicons/');
		if ($deleteDir->delete()) {
			$this->response->body(json_encode(['success' => true]));
			$this->response->statusCode(200);
		}else {
			$this->response->statusCode(403);
		}

		$this->response->send();
		$this->response->stop();
	}

    public function deletehomepic()
    {
        $this->response->type('json');

        $deleteDir = new \Cake\Filesystem\Folder(WWW_ROOT.'img/homepic/');
        if ($deleteDir->delete()) {
            $this->response->body(json_encode(['success' => true]));
            $this->response->statusCode(200);
        }else {
            $this->response->statusCode(403);
        }

        $this->response->send();
        $this->response->stop();
    }

	public function clearCache(){
		$this->autoRender = false;
		$clear = \Cake\Cache\Cache::clearAll(false);
		echo json_encode(['success'=>$clear]);
		die();
	}
}
