<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;

/**
 * ActionsModules Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Actions
 * @property \Cake\ORM\Association\BelongsTo $Modules
 *
 * @method \App\Model\Entity\ActionsModule get($primaryKey, $options = [])
 * @method \App\Model\Entity\ActionsModule newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ActionsModule[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ActionsModule|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ActionsModule patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ActionsModule[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ActionsModule findOrCreate($search, callable $callback = null, $options = [])
 */
class ActionsModulesTable extends AbstractTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('actions_modules');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Actions', [
            'foreignKey' => 'action_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Modules', [
            'foreignKey' => 'module_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['action_id'], 'Actions'));
        $rules->add($rules->existsIn(['module_id'], 'Modules'));

        return $rules;
    }
}
