<?php
namespace App\Model\Behavior;

use Cake\ORM\Behavior;
use Cake\ORM\Table;

use Cake\Event\Event;
use Cake\Datasource\EntityInterface;
use Cake\Utility\Inflector;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Cake\ORM\Query;

/**
 * Sluggable behavior
 */
class SearchBehavior extends Behavior
{

    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [
        'fields' => [],
    ];


    public function beforeFind(Event $event, Query $query, $options)
    {
		if(defined('CONTROLLER_NAME') && defined('ACTION_NAME') && CONTROLLER_NAME == str_replace(['_'],'',$event->subject()->getTable()) && ACTION_NAME == 'index') {
			$search = $_GET['search'];
            $config = $this->config();
            $subject = $event->subject();
            $table = str_replace(['_'],'',$event->subject()->getTable());
            $sql = '(';
            foreach ($config['fields'] as $k => $field) {
                if ($subject->associations()->get($table . '_' . $field . '_translation')) {
                    $field = $event->subject()->translationField($field);
                }
                $sql .= " $field LIKE '%$search%' ";
                $sql .= count($config['fields']) == $k + 1 ? "" : " OR ";
            }
            $sql .= ')';
            $query->andWhere($sql);
        }
    }

}
