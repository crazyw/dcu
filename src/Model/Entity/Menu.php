<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\Behavior\Translate\TranslateTrait;

/**
 * Menu Entity
 *
 * @property int $id
 * @property bool $published
 * @property int $position
 */
class Menu extends AbstractEntity
{
    use TranslateTrait;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

	public function getTitle(){
		return $this->page && $this->page->title ? $this->page->title : $this->title;
	}

	public function getLazyImage($column)
    {
        $image = $this->{$column};
        $image_folder = $this->getImageItem($column,'big_crop','Menus');
        if(is_file(WWW_ROOT.$image_folder)){
            $file_name = WWW_ROOT.pathinfo($image_folder)['dirname'].DS.md5(pathinfo($image_folder)['filename']).'.jpg';
            if(!is_file($file_name)){
                $this->png2jpg(WWW_ROOT.$image_folder,$file_name,10);
            }
            return pathinfo($image_folder)['dirname'].DS.md5(pathinfo($image_folder)['filename']).'.jpg';
        }
        return '';
    }

    private function png2jpg($originalFile, $outputFile, $quality = 100)
    {
        $image = imagecreatefrompng($originalFile);
        imagejpeg($image, $outputFile, $quality);
        imagedestroy($image);
    }

    public function getHref(){
		$href = '';
		if($this->type == 1 && $this->page){
			$ex = explode('/',$this->page->module);
			if(!empty($ex)) {
				$url = [];
				$url['controller'] = $ex[0];
				$url['language'] = LANG;
				$url['action'] = 'index';
				if (count($ex) == 2) {
					$url['action'] = $ex[1];
					if(isset($this->page->slug[LANG])){
						$url['slug'] = $this->page->slug[LANG];
					}
				}
				$href = \Cake\Routing\Router::url($url);
			}
		}elseif($this->type == 1){
			$href = $this->external_link;
		}
		return $href;
	}
}
