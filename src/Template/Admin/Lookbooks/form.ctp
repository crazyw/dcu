<?php
	echo $this->element('header', ['itemPage' => true,'langSwitcher'=>true]);
	echo $this->Html->div('content-block',
		$this->Form->create($page, ['class' => "form-page"])
		.
		$this->Form->control('id',['type' => 'hidden'])
		.
		$this->Html->div('row',
			$this->Form->control('title',[
				'value'=>$page,
				'lang'=>true,
				'label'=>false,
				'templates' => ['inputContainer' => '{{content}}'],
				'templateVars' => [
					'label'=>__da('Title'),
					'class' => 'col-sm-12',
				]
			])
		)
        .
        $this->Html->div('row',
            $this->Form->control('price', [
                'lang' => true,
                'label' => false,

                'templates' => ['inputContainer' => '{{content}}'],
                'templateVars' => [
                    'label' => __da('Price'),
                    'class' => 'col-sm-12',
                    'inputclass' => 'title-input'
                ]
            ])
        )
        .
        $this->Html->div('row',
            $this->Form->control('fabric', [
                'lang' => true,
                'label' => false,

                'templates' => ['inputContainer' => '{{content}}'],
                'templateVars' => [
                    'label' => __da('Fabric'),
                    'class' => 'col-sm-12',
                    'inputclass' => 'title-input'
                ]
            ])
        )
        .
        $this->Html->div('row',
            $this->Form->control('content',[
                'lang'=>true,
                'type' => 'textarea',
                'label'=>false,
                'templates' => ['inputContainer' => '{{content}}'],
                'templateVars' => [
                    'label'=>__da('Content'),
                    'class' => 'col-sm-12',
                    //'class' => 'col-sm-12 tinymce',
                ]
            ])
        )
        .

        $this->Html->div('row',
            $this->Form->control('image', [
                'type' => 'hidden',
                'templateVars' => [
                    'label' => __da('Image'),
                    'class' => 'col-sm-3'
                ]
            ])
        )
        .
		$this->Html->div('row',
			$this->Form->control('published',[
				'type' => 'checkbox',
				'templateVars' => [
					'label'=>__da('Published'),
					'class' => 'col-sm-6 checkbox-block',
				]
			])
			.
			$this->Form->control(__da('Save and continue'),[
				'type'=>'submit',
				'ajax'=>true,
				'templateVars' => [
					'class' => 'btn btn-grey btn-large pull-right save-continue',
					'divclass'=>'col-block col-sm-5 col-save-buttons',
				]
			])
			.
			$this->Form->control(__da('Save'),[
				'type'=>'submit',
				'ajax'=>true,
				'templateVars' => [
					'class' => 'btn btn-green btn-large pull-right save-redirect',
					'divclass'=>'col-block col-sm-1 col-save-buttons'
				]
			])
		)
		.
		$this->Form->end()
	);
?>
