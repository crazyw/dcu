<ol class="
    <?php if($first_level) echo 'sortable'?>
">
    <?php foreach($menus as $menu){?>
        <li <?php if(isset($menu['Menus']['published']) && $menu['Menus']['published'] == false) echo 'class="unpublished"'?>>
            <div>
                <a href="#"><?php echo $menu['Menus']['_translations'][LANG]['title']?></a>
                <span class="table-actions">
                    <?php
                    // actions
                        echo $this->element('index/actions', [
                            'item' => $menu['Menus'],
                            'actions' => ['edit' => true, 'delete'  => true, 'publish' => true],
                        ] );
                    ?>
                </span>
            </div>
            <?php  if(isset($menu['Childrens']) && count($menu['Childrens']))
                echo $this->element('../Menus/table', ['menus' => $menu['Childrens'], 'first_level' => false])
            ?>
        </li>
    <?php }?>
</ol>
