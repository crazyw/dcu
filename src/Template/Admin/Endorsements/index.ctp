<?php

echo $this->element('header', ['header' => false]);

echo $this->element('index/table',[
	'model_name' => MODEL_NAME,
	'items' => ${Cake\Utility\Inflector::variable(CONTROLLER_NAME)},
	'fields' => [
		[
			'checkboxes' => true,
		],
		[
			'name' => 'name',
			'view_name' => __da('Name'),
			'is_link' => true,
			'class' =>'col-md-8'
		],
		[
			'actions' => ['edit', 'delete', 'publish'],
		],
	],
]);
?>



