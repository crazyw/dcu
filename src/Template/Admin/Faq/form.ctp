<?php
echo $this->element('header', ['itemPage' => true,'langSwitcher'=>true]);

echo $this->Html->div('content-block',
	$this->Form->create($page, ['class' => "form-page"])
	.
	$this->Form->control('id',['type' => 'hidden'])
	.
	$this->Html->div('row',
		$this->Form->control('title',[
			'lang'=>true,
			'label'=>false,
			'templates' => ['inputContainer' => '{{content}}'],
			'templateVars' => [
				'label'=>__da('Title'),
				'class' => 'col-sm-12',
			]
		])
	)
	.
	$this->Html->div('row',
		$this->Form->control('content',[
			'lang'=>true,
			'type' => 'textarea',
			'label'=>false,
			'templates' => ['inputContainer' => '{{content}}'],
			'templateVars' => [
				'label'=>__da('Content'),
				'class' => 'col-sm-12 tinymce',
			]
		])
	)
	.
	$this->element('index/save_actions')
	.
	$this->Form->end()
);
?>