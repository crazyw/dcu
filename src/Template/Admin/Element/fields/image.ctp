<?php if((dev_conf('Config.singleImage.'.MODEL_NAME) || dev_conf('Config.galeryModels.'.MODEL_NAME) || dev_conf('Config.fileModels.'.MODEL_NAME)) && isset($page)){?>

    <?php if(dev_conf('Config.singleImage.'.MODEL_NAME) || dev_conf('Config.galeryModels.'.MODEL_NAME)){?>
        <link rel="stylesheet" href="/admin-assets/plugin/cropperjs/dist/cropper.min.css"/>
        <script src="/admin-assets/plugin/cropperjs/dist/cropper.min.js"></script>

        <div class="modal fade" id="cropperModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <form action="" id="cropForm">
                <input type="hidden" name="id" value="<?php echo $page->id ?>">
                <input type="hidden" name="crop[filename]" class="crop-current-data">
                <input type="hidden" name="crop[x]" class="crop-current-data">
                <input type="hidden" name="crop[y]" class="crop-current-data">
                <input type="hidden" name="crop[width]" class="crop-current-data">
                <input type="hidden" name="crop[height]" class="crop-current-data">
                <input type="hidden" name="crop[rotate]" class="crop-current-data">
                <input type="hidden" name="crop[scaleX]" class="crop-current-data">
                <input type="hidden" name="crop[scaleY]" class="crop-current-data">

                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabelImage"></h4>
                        </div>
                        <div class="modal-body" style="width: 568px;">
                            <img src="" alt="" id="cropImage" style="max-width: 568px;">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo __da('Close'); ?></button>
                            <button type="submit" class="btn btn-primary btn-modal-image-save"><?php echo __da('Save'); ?></button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    <?php } ?>

    <?php if(dev_conf('Config.galeryModels.'.MODEL_NAME) || dev_conf('Config.fileModels.'.MODEL_NAME)){
        $mulltiple_image = dev_conf('Config.singleImage.'.MODEL_NAME.'.gallery.multiple');
        $mulltiple_files = dev_conf('Config.'.MODEL_NAME.'.fileFields.files.multiple');
        ?>
        <form class="gallery-form" method="POST">
            <input type="hidden" name="random[gallery]" id="gallery-random" value="<?php echo uniqid(); ?>"/>
            <input type="hidden" name="random[files]" id="files-random" value="<?php echo uniqid(); ?>"/>
            <div class="right-sidebar">
                <div class="hide-scroll">
                    <div class="right-sidebar-wrap">
                        <div class="tabs tabs-sidebar tabs-2">
                            <input type="file" name="photo[gallery]<?php echo $mulltiple_image ? '[]' : '';?>" style="display: none;" class="image-file" <?php echo $mulltiple_image ? 'multiple' : '';?>>
                            <ul role="tablist" class="nav nav-tabs" id="tab-Files">
                                <?php if(dev_conf('Config.galeryModels.'.MODEL_NAME)){?>
                                    <li class="active" role="presentation">
                                        <a aria-controls="dropdown1" data-toggle="tab" role="tab" href="#dropdown1" aria-expanded="true"><?php echo __da('Gallery'); ?></a>
                                    </li>
                                <?php } ?>
                                <?php if(dev_conf('Config.fileModels.'.MODEL_NAME)){?>
                                    <li role="presentation" class="<?php echo dev_conf('Config.galeryModels.'.MODEL_NAME) ? '' : 'active'; ?>">
                                        <a aria-controls="dropdown2" data-toggle="tab" role="tab" href="#dropdown2" aria-expanded="true"><?php echo __da('Files'); ?></a>
                                    </li>
                                <?php } ?>
                            </ul>
                            <div class="tab-content" id="myTabContent">
                                <?php if(dev_conf('Config.galeryModels.'.MODEL_NAME)){?>
                                    <div aria-labelledby="home-tab" id="dropdown1" class="tab-pane fade active in" role="tabpanel">
                                        <button class="btn btnAddItem btn-medium btn-default btn-center gallery"><?php echo __da('Upload image'); ?></button>
                                        <?php if($page->images){?>
                                            <?php foreach ($page->images as $image){?>
                                                <div class="right-element">
                                                    <div class="right-element-img">
                                                        <a class="right-element-link" href="" rel="search-results-gallery">
                                                            <img class="grid-img" src="<?php echo $image->getImageItem('gallery') ?>">
                                                        </a>
                                                        <button class="remove-element" onclick="deleteGalleryImage(this,'isset',<?php echo $image->id;?>); return false;"><i class="icon-trash-o"></i></button>
                                                    </div>
                                                    <input type="hidden" name="gallery[<?php echo $image->id; ?>]" value="<?php echo $image->name; ?>">
                                                    <!-- <input class="form-control" type="text"> -->
                                                </div>
                                            <?php }?>
                                        <?php } ?>
                                    </div>
                                <?php } ?>
                                <?php if(dev_conf('Config.fileModels.'.MODEL_NAME)){?>
                                    <div aria-labelledby="home-tab" id="dropdown2" class="tab-pane fade in <?php echo dev_conf('Config.galeryModels.'.MODEL_NAME) ? '' : 'active'; ?>" role="tabpanel">
                                        <input type="hidden" name="files" value="files" class="hidden file_name">
                                        <input type="file" name="file_upload[files]<?php echo $mulltiple_files ? '[]' : '';?>" class="hidden file_upload_trigger" <?php echo $mulltiple_image ? 'multiple' : '';?>>
                                        <button class="btn btn-medium btn-default btn-center file_upload" data-multiple="1"><?php echo __da('Upload file'); ?></button>
                                        <?php if($page->files){?>
                                            <?php foreach ($page->files as $file){?>
                                                <div class="right-file-element">
                                                    <a href="<?php echo '/'.FILE_PATH_WEB.strtolower(MODEL_NAME).'/files/'.$file->id.'/'.$file->name; ?>" target="_blank" class="file-type"><i class="icon-file-word-o"></i></a>
                                                    <span class="file-title"><?php echo $file->title ?></span>
                                                    <span class="remove-file" onclick="deleteFile(this,<?php echo $file->id?>,'files')"><i class="icon-remove"></i></span>
                                                    <input type="hidden" name="file_list[<?php echo $file->id;?>]" value="<?php echo $file->name;?>">
                                                    <span class="file-title-edit"><input name="file_title[<?php echo $file->id;?>]" value="<?php echo $file->title ?>" class="form-control" type="text"></span>
                                                </div>
                                            <?php } ?>
                                        <?php } ?>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    <?php } ?>
    <script src="/common/js/lodash.min.js"></script>

    <script src="/common/js/jquery.form.js"></script>

    <!--    Шаблон для галереи-->
    <script type="text/template" id="galleryTmp">
        <div class="right-element">
            <div class="right-element-img">
                <a class="right-element-link" href="" rel="search-results-gallery">
                    <img class="grid-img" src="<%= image %>">
                </a>
                <button class="remove-element" onclick="deleteGalleryImage(this,'new','<%= random %>','gallery'); return false;"><i class="icon-trash-o"></i></button>
            </div>
            <input type="hidden" name="gallery[<%= random %>]" value="<%= filename %>">
            <!-- <input class="form-control" type="text"> -->
        </div>
    </script>

    <!--    Шаблон для файлов -->
    <script type="text/template" id="filesTmp">
        <div class="right-file-element">
            <a href="#" class="file-type"><i class="icon-file-word-o"></i></a>
            <span class="file-title"><%= filename %></span>
            <span class="remove-file" onclick="deleteFile(this,'<%= random %>','files')"><i class="icon-remove"></i></span>
            <input type="hidden" name="file_list[<%= random %>]" value="<%= filename %>">
            <span class="file-title-edit"><input name="file_title[<%= random %>]" value="<%= filename %>" class="form-control" type="text"></span>
        </div>
    </script>

    <!--    Шаблон для обычной загрузки-->
    <script type="text/template" id="imageTmp">
        <div class="image">
            <img src="<%= image %>" alt="">
        </div>
        <button class="btnDeleteItem"><i class="icon-trash-o"></i></button>
    </script>
    <script src="/admin-assets/js/image.js"></script>
<?php } ?>
