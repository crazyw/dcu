<select name="<?php echo $name ?>" class="select2-templating">
<!--    <option value="0">---</option>-->
    <?php foreach (getInternalSvgIcon(isset($path) ? $path : 'svg') as $icon): ?>
        <option value="<?php echo $icon ?>" <?php echo $page->{$name} == $icon ? 'selected' : '' ?>><?php echo $icon ?></option>
    <?php endforeach; ?>
</select>