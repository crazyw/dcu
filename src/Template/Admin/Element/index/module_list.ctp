<div class="row">
	<?php
	foreach ($page->update_plugins as $plugin){?>
		<div class="col-md-2">
				<label for="plugin-<?php echo $plugin->id ?>"><?php echo $plugin->title; ?></label>
				<input type="hidden" name="update_plugins[<?php echo $plugin->id ?>][id]" value="<?php echo $plugin->id ?>">
				<input type="hidden" name="update_plugins[<?php echo $plugin->id ?>][active]" value="0">
				<input type="checkbox" name="update_plugins[<?php echo $plugin->id ?>][active]"  value="1" <?php echo $plugin->active == 1 ? 'checked' : '' ?> id="plugin-<?php echo $plugin->id ?>">
		</div>
	<?}?>
</div>
