<?php if ( isset($permissions[MODEL_NAME]['edit']) || $superUser ) { ?>
<a href="<?php echo $this->Url->build(['controller'=>$this->request->getParam('controller'),'action'=>'add','prefix'=>'admin']); ?>" class="btn btn-large btn-green btn-add" id="btnAdd">
    <i class="icon-plus"></i> <?php echo __da('Add') ?>
</a>
<?php } ?>