<!DOCTYPE html>
<html class="maintenance-page">
    <head>
        <?= $this->Html->charset() ?>

        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
        <meta name="format-detection" content="telephone=no">
        <meta name="theme-color" content="#6C38A0">

        <?= $this->element('meta') ?>
        <?= $this->element('favicons') ?>

        <link type="text/css" rel="stylesheet" href="/common/maintenance/css/normalize.css?v=1">

        <!-- Client CSS -->
        <link type="text/css" rel="stylesheet" href="/common/maintenance/css/styles.css?v=2">

        <?php if (conf('Google.analitics') != null): ?>
            <?= conf('Google.analitics') ?>
        <?php endif; ?>

        <?php if (conf('Google.remarketing') != null): ?>
            <?= conf('Google.remarketing') ?>
        <?php endif; ?>

    </head>

    <body>
        <div class="wrapper">
            <div class="main-block">
                <div class="main-wrap">
                    <div class="cover-container clearfix">

                        <a class="logo" href="<?= $this->Url->build(['controller' => 'Pages', 'action' => 'display', 'language' => LANG]) ?>"><img src="/img/logo.svg" alt="<?= conf('Config.projectName-'.LANG) ?>"></a>

                        <h3><?= conf('Maintenance.message-'.LANG) ?></h3>
                        <div class="contacts">
                            <?php if (conf('Contact.email') != null) { ?>
                                <a href="mailto:<?= conf('Contact.email') ?>"><?= conf('Contact.email') ?></a>
                            <?php } ?>
                            <?php if (conf('Contact.phone') != null) { ?>
                                <?php $phone = explode(',', conf('Contact.phone')) ?>
                                <a href="tel:<?= isset($phone[0]) ? $phone[0] : '' ?>"><?= isset($phone[0]) ? $phone[0] : '' ?></a>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>



