<!DOCTYPE html>
<html lang="en" class="login-page">

<?php echo $this->element('../Admin/Element/head') ?>

<body>
	<div class="login-wrapper">
		<div class="login-wrapper-inner">

			<div class="center-block">
				<div class="login-block">
					<?= $this->fetch('content') ?>
				</div>
				<div class="footer-block">
					<span>Developed by <a href="http://amigo.md" target="_blank" title="Amigo.md">Amigo</a></span>
				</div>

			</div>

		</div>
	</div>
</body>
</html>
