<ul class="nav nav-tabs nav-underline" role="tablist">
	<li role="presentation" class="<?php echo !isset($this->request->params['tab']) ? 'active': '' ?>">
		<a href="#biografie" aria-controls="biografie"
		   data-href="<?php echo $this->Url->build(['controller'=>CONTROLLER_NAME, 'action'=>ACTION_NAME,'slug'=>$page->slug[LANG],'language'=>LANG]);?>"
		   role="tab"
		   data-toggle="tab">
			<?php if(CONTROLLER_NAME == 'members'){?>
				Biografie
			<?php }else{?>
				<?php echo __ds('Descriere') ?>
			<?php } ?>
		</a>
	</li>
	<li role="presentation" class="<?php echo isset($this->request->params['tab']) && $this->request->params['tab'] == 'filmografie' ? 'active': '' ?>">
		<a href="#filmografie"
		   data-href="<?php echo $this->Url->build(['controller'=>CONTROLLER_NAME, 'action'=>ACTION_NAME,'slug'=>$page->slug[LANG],'tab'=>'filmografie','language'=>LANG]);?>"
		   aria-controls="filmografie"
		   role="tab"
		   data-toggle="tab">Filmografie</a>
	</li>
	<li role="presentation" class="<?php echo isset($this->request->params['tab']) && $this->request->params['tab'] == 'festivaluri' ? 'active': '' ?>"><a href="#festivaluri" data-href="<?php echo $this->Url->build(['controller'=>CONTROLLER_NAME, 'action'=>ACTION_NAME,'slug'=>$page->slug[LANG],'tab'=>'festivaluri','language'=>LANG]);?>" aria-controls="festivaluri" role="tab" data-toggle="tab">Festivaluri</a></li>
	<li role="presentation" class="<?php echo isset($this->request->params['tab']) && $this->request->params['tab'] == 'recenzii' ? 'active': '' ?>"><a href="#recenzii" data-href="<?php echo $this->Url->build(['controller'=>CONTROLLER_NAME, 'action'=>ACTION_NAME,'slug'=>$page->slug[LANG],'tab'=>'recenzii','language'=>LANG]);?>" aria-controls="recenzii" role="tab" data-toggle="tab">Recenzii</a></li>
	<li role="presentation" class="<?php echo isset($this->request->params['tab']) && $this->request->params['tab'] == 'echipament' ? 'active': '' ?>"><a href="#echipament" data-href="<?php echo $this->Url->build(['controller'=>CONTROLLER_NAME, 'action'=>ACTION_NAME,'slug'=>$page->slug[LANG],'tab'=>'echipament','language'=>LANG]);?>" aria-controls="echipament" role="tab" data-toggle="tab">Echipament</a></li>
</ul>
<script>
	$(function () {
		$('.nav-tabs.nav-underline li a').click(function (e) {
			e.preventDefault();
			link = $(this).attr('data-href');
			history.pushState({myTag: link}, "", link);
		});
		$(window).on("popstate", function (e) {
			window.location.reload();
		});
	})
</script>
