<?php
//$title_for_layout = $page_title == 'Errors' ? __($page_title) : $page_title;
if(!defined('LANG')){
    define('LANG','en');
}
$title = isset($page_title) ? ($page_title ? $page_title . '' : '') : conf('Config.projectName-'.LANG);
$baseUrl = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://':'http://').$_SERVER['HTTP_HOST'].'/';
$url = $baseUrl . seoify();
$description = !empty($description) ? $description : conf('Meta.description-' . LANG);
$og_image = !empty($og_image) ? $og_image : dev_conf('Config.fullUrl').'img/share.jpg';
?>

<title><?php echo isset($title) ? summarize($title,70) : ''; ?></title>
<base href="<?php echo (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://':'http://').$_SERVER['HTTP_HOST'].'/' ?>">
<meta name="description" content="<?php echo escape_meta(summarize($description, 152)) ?>">

<?php if(isset($meta_keywords) && trim($meta_keywords)):?>
    <meta name="keywords" content="<?php echo $meta_keywords ?>" />
<?php endif; ?>
<!-- Open Graph -->
<meta property="og:type" content="website">
<meta property="og:site_name" content="<?php echo escape_meta(conf('Config.projectName-'.LANG)) ?>">
<meta property="og:title" content="<?php echo escape_meta($title) ?>">
<meta property="og:description" content="<?php echo escape_meta(summarize($description, 456)) ?>">
<meta property="og:url" content="<?php echo $url ?>">
<meta property="og:image" content="<?php echo $og_image ;?>">

<link rel="canonical" href="<?php echo $url ?>">
<?php $default = dev_conf('Languages.'.dev_conf('Settings.Language.front')) ?>
<link href="<?php echo $this->Url->build([
    'controller'=>CONTROLLER_NAME,
    'action'=>ACTION_NAME,
    'slug'=> isset($translatedSlugs[$default->code]) ? $translatedSlugs[$default->code] : false,
    'language' => ($default->code == 'rom' ? null : $default->code)],true);
?>" hreflang="x-default" rel="alternate">

<?php foreach (dev_conf('Languages') as $language):
    if(!$language->site) continue;
    if((CONTROLLER_NAME == 'comparisons' && ACTION_NAME == 'index' && isset($translatedSlugs[$language->code])) || CONTROLLER_NAME != 'comparisons'):?>
    <link rel="alternate" href="<?php echo $this->Url->build([
        'controller'=>CONTROLLER_NAME,
        'action'=>ACTION_NAME,
        'slug'=> isset($translatedSlugs[$language->code]) ? $translatedSlugs[$language->code] : false,
        'language' => ($language->code == 'rom' ? null : $language->code)],true);
    ?>" hreflang="<?php echo $language->code ?>">
    <?php endif; ?>
<?php endforeach; ?>
