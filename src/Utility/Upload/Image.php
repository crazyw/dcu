<?php
namespace App\Utility\Upload;

use Cake\Filesystem\Folder;
use Cake\Filesystem\File;

class Image {

	private $settings;
	private $module;
	private $path;
	private $unique_id;

	public function setUniqueId($unique_id){
		$this->unique_id = $unique_id;
	}

	public function setPath($path){
		$this->path = $path;
	}

	public function getPathWeb(){
		return IMAGE_PATH_WEB.$this->path.DS;
	}

	public function setSettings($settings){
		$this->settings = $settings;
	}

	public function setModule($module){
		$this->module = $module;
	}

	public function UploadFromExpernal($file)
	{
		$filename = $this->upload($file);
		$this->createThumbs($filename);

		$out = [];
		$out['filename'] = $filename;
		$out['path'] = IMAGE_PATH_WEB.$this->path;
		$out['crop'] = isset($this->settings['crop']['active']) && $this->settings['crop']['active'] ? true : false;
		$out['aspectRatio'] = isset($this->settings['crop']['aspectRatio']) ? $this->settings['crop']['aspectRatio'] : 16/9;
		return $out;
	}

	public function upload($file) {
		//Существует ли файл
		$folder = IMAGE_PATH.$this->path;
		if (empty($file) || empty($file['tmp_name'])) {
			$this->error = 'Файл пуст.';
			return false;
		}

		//Удаление лишних символов и пробелов
		$newFileName = empty($filename) ? $file['name'] : $filename;
		$newFileName = preg_replace("/[^\da-z.]/i", "_", $newFileName);

		//Проверка если есть уже такой файл на сервере
		$verifyDestinationImage = new File($folder.DS.$newFileName);
		if($verifyDestinationImage->exists()){
			$newFileName = uniqid().'_'.$newFileName;
		}

		// Создание папки и загрузка картинки на сервер
		$dir = new Folder();
		if ($dir->create($folder,0777)) {
			$tmp_file = new File($file['tmp_name']);
			if(!$tmp_file->exists()){
				$this->error = 'Файл пуст.';
				return false;
			}
			$new_file = new File($folder . DS . $newFileName);
			if (!$tmp_file->copy($folder . DS . $newFileName)) {
				$this->error = 'Не удалось переместить загруженный файл в каталог.';
				return false;
			}
			$new_file->close();
//			$tmp_file->delete();
			return $newFileName;
		}else{
			$this->error = 'Временная папка не создана.';
		}
	}

	public function createThumbs($filename){
		$source_image = IMAGE_PATH.$this->path . DS . $filename;

		if (isset($this->settings['thumbs']) && !empty($this->settings['thumbs'])) {
			foreach ($this->settings['thumbs'] as $dir => $thumb) {
				$destination = IMAGE_PATH.$this->path . DS . $dir;

				$createDest = new \Cake\Filesystem\Folder();
				if ($createDest->create($destination, 0777)) {
					if (!empty($thumb['width']) && empty($thumb['height'])) {
						$this->resizeByWidth($source_image, $thumb['width'], $destination);
					} elseif (empty($thumb['width']) && !empty($thumb['height'])) {
						$this->resizeByHeight($source_image, $thumb['height'], $destination);
					} elseif (!empty($thumb['width']) && !empty($thumb['height'])) {
						$this->smartCrop($source_image, $thumb['width'], $thumb['height'], $destination);
					}
				}
			}
		}
	}

	public function ValidateUpload(&$out)
	{
		$out['error'] = true;

		if(!isset($_POST['options']['module']) || !isset($_POST['options']['name'])){
			$out['message'] = 'Недостаточно данных';
			return false;
		}

		if(!isset($_FILES['photo'])) {
			$out['message'] = 'Не удалось найти загруженное фото';
			return false;
		}

		$settings = dev_conf('Config.singleImage.'.ucfirst($_POST['options']['module']).'.'.$_POST['options']['name']);
		if(!$settings){
			$out['message'] = 'Не удалось найти настройки';
			return false;
		}
		$this->unique_id = $_POST['random'][$_POST['options']['name']];
		$this->settings = $settings;
		$this->module = $_POST['options']['module'];
		$this->path = $this->module.DS.$_POST['options']['name'].DS.IMAGE_PATH_TMP.DS.$this->unique_id;
		$normalize = $this->FilesNormalize();
		foreach ($normalize as $item){
			$error = $this->getUploadErrors($item,$settings);
			if(!empty($error)){
				$out['message'] = $error[0];
				return true;
			}
		}
		$out['error'] = false;
		return $settings;
	}


	public function FilesNormalize(){
		foreach ($_FILES['photo'] as $key => $photo) {
			foreach ($photo as $prop => $value) {
				$files[$prop][$key] = $value;
			}
		}
		$files[0] = $files[$_POST['options']['name']];
		unset($files[$_POST['options']['name']]);
		return $files;
	}

	public function getImageDimension($file){
		if(is_file($file)) {
			$size = getimagesize($file);
			$sizes['w'] = $size[0];
			$sizes['h'] = $size[1];
		}else{
			$sizes['w'] = 0;
			$sizes['h'] = 0;
		}
		return $sizes;
	}

	public function getSize($path, $human = false) {
		if (!is_file($path)) {
			$this->error = 'Не удалось вычислить размер изображения (' . $path . '). Файл не существует.';
			return false;
		}

		$bytes = filesize($path);

		if ($human) {
			return $this->humanFilesize($bytes);
		}

		return $bytes;
	}

	private function humanFilesize($bytes, $decimals = 2) {
		//$sz = 'BKMGTP';
		$sz = array('bytes', 'KB', 'MB', 'GB', 'TB', 'PB');
		$factor = floor((strlen($bytes) - 1) / 3);
		return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . ' ' . @$sz[$factor];
	}

	public function resizeByWidth($srcPath, $newWidth, $dstPath = null, $stretch = false) {
		$imageSize = $this->getImageSize($srcPath);
		if (empty($imageSize)) {
			return false;
		}
		$scale = $newWidth / $imageSize[0];

		if (empty($dstPath)) {
			$dstPath = $srcPath;
		}

		if ($stretch == false && $newWidth >= $imageSize[0]) {
			$this->copy($srcPath, $dstPath);
			return array('width' => $imageSize[0], 'height' => $imageSize[1]);
		}

		return $this->resizeByScale($srcPath, $scale, $dstPath);
	}

	public function resizeByHeight($srcPath, $newHeight, $dstPath = null, $stretch = false) {
		$imageSize = $this->getImageSize($srcPath);
		if (empty($imageSize)) {
			return false;
		}
		$scale = $newHeight / $imageSize[1];

		if (empty($dstPath)) {
			$dstPath = $srcPath;
		}

		if ($stretch == false && $newHeight >= $imageSize[1]) {
			$this->copy($srcPath, $dstPath);
			return array('width' => $imageSize[0], 'height' => $imageSize[1]);
		}

		$this->resizeByScale($srcPath, $scale, $dstPath);
	}

	public function resizeByScale($srcPath, $scale, $dstPath = null, $stretch = false) {
		$imageSize = $this->getImageSize($srcPath);
		if (empty($imageSize)) {
			return false;
		}
		$imageType = $imageSize[2];

		$newWidth = round($imageSize[0] * $scale, 0);
		$newHeight = round($imageSize[1] * $scale, 0);

		switch ($imageType) {
			case IMAGETYPE_JPEG: $srcImage = imagecreatefromjpeg($srcPath);
				break;
			case IMAGETYPE_PNG: $srcImage = imagecreatefrompng($srcPath);
				break;
			case IMAGETYPE_GIF: $srcImage = imagecreatefromgif($srcPath);
				break;
			default:
				$this->error = 'Неверный тип изображения: ' . $imageType;
				return false;
		}

		$newImage = imagecreatetruecolor($newWidth, $newHeight);
		imagealphablending($newImage, false);
		imagesavealpha($newImage, true);
		imagecolorallocatealpha($newImage, 255, 255, 255, 127);
		imagecopyresampled($newImage, $srcImage, 0, 0, 0, 0, $newWidth, $newHeight, $imageSize[0], $imageSize[1]);

		if (empty($dstPath)) {
			$dstPath = $srcPath;
		} elseif (is_dir($dstPath)) {
			$dstPath .= '/' . basename($srcPath);
		}

		switch ($imageType) {
			case IMAGETYPE_JPEG: imagejpeg($newImage, $dstPath, dev_conf('Config.jpgQuality'));
				break;
			case IMAGETYPE_PNG: imagepng($newImage, $dstPath, dev_conf('Config.pngCompression'));
				break;
			case IMAGETYPE_GIF: imagegif($newImage, $dstPath);
				break;
		}

		imagedestroy($srcImage);
		imagedestroy($newImage);

		return array('width' => $newWidth, 'height' => $newHeight);
	}

	function smartCrop($srcPath, $newWidth, $newHeight, $dstPath = null) {
		if (empty($dstPath)) {
			$dstPath = $srcPath;
		} elseif (is_dir($dstPath)) {
			$dstPath .= '/' . basename($srcPath);
		}

		$imageSize = $this->getImageSize($srcPath);
		if (empty($imageSize)) {
			return false;
		}

		$resizeByWidthScale = $newWidth / $imageSize[0];
		$resizeByHeightScale = $newHeight / $imageSize[1];
		$scale = $resizeByWidthScale > $resizeByHeightScale ? $resizeByWidthScale : $resizeByHeightScale;

		$this->resizeByScale($srcPath, $scale, $dstPath);
		$imageSize = getimagesize($dstPath);

		// если надо режем сверху и снизу
		if ($resizeByWidthScale < $resizeByHeightScale) {
			$x = ($imageSize[0] - $newWidth) / 2;
			$y = 0;
		} else {
			$x = 0;
			$y = ($imageSize[1] - $newHeight) / 2;
		}

		return $this->crop($dstPath, $x, $y, $newWidth, $newHeight);
	}

	public function crop($srcPath, $x, $y, $w, $h, $dstPath = null) {
		$w = round($w, 0);
		$h = round($h, 0);

		if (empty($dstPath)) {
			$dstPath = $srcPath;
		} elseif (is_dir($dstPath)) {
			$dstPath .= '/' . basename($srcPath);
		}

		$imageSize = $this->getImageSize($srcPath);
		if (empty($imageSize)) {
			return false;
		}
		$imageType = $imageSize[2];

		switch ($imageType) {
			case IMAGETYPE_JPEG: $srcImage = imagecreatefromjpeg($srcPath);
				break;
			case IMAGETYPE_PNG: $srcImage = imagecreatefrompng($srcPath);
				break;
			case IMAGETYPE_GIF: $srcImage = imagecreatefromgif($srcPath);
				break;
			default:
				$this->error = 'Неверный тип изображения: ' . $imageType;
				return false;
		}

		$newImage = imagecreatetruecolor($w, $h);
		imagealphablending($newImage, false);
		imagesavealpha($newImage, true);
		imagecolorallocatealpha($newImage, 255, 255, 255, 127);
		imagecopyresampled($newImage, $srcImage, 0, 0, $x, $y, $w, $h, $w, $h);

		switch ($imageType) {
			case IMAGETYPE_JPEG: imagejpeg($newImage, $dstPath, 100);
				break;
			case IMAGETYPE_PNG: imagepng($newImage, $dstPath, 0);
				break;
			case IMAGETYPE_GIF: imagegif($newImage, $dstPath);
				break;
		}

		imagedestroy($srcImage);
		imagedestroy($newImage);
		return true;
	}

	protected function getUploadErrors($file,$config){
		$errors = [];
		$size = $this->getImageDimension($file['tmp_name']);
		$file_extension = mb_strtolower(pathinfo($file['name'], PATHINFO_EXTENSION));
		if(isset($config['avalaibleExtension']) && !empty($config['avalaibleExtension'])){
			if(!in_array($file_extension,$config['avalaibleExtension'])){
				$errors[] = __da('Формат файла неверный. Список доступных для загрузки: ').implode(' , ',$config['avalaibleExtension']);
			}
		}
		if(isset($config['upload'])){
			if(isset($config['upload']['maxWidth']) && $size['w'] > $config['upload']['maxWidth']){
				$errors[] = __da('Ширина картинки должна быть меньше ').$config['upload']['maxWidth'];
			}
			if(isset($config['upload']['maxHeight']) && $size['h'] > $config['upload']['maxHeight']){
				$errors[] = __da('Высота картинки должна быть меньше ').$config['upload']['maxHeight'];
			}
			if(isset($config['upload']['minWidth']) && $size['w'] < $config['upload']['minWidth']){
				$errors[] = __da('Ширина картинки должна быть больше ').$config['upload']['minWidth'];
			}
			if(isset($config['upload']['minHeight']) && $size['h'] < $config['upload']['minHeight']){
				$errors[] = __da('Высота картинки должна быть больше ').$config['upload']['minHeight'];
			}
			if(isset($config['upload']['maxSize'])){
				$maxSize = preg_replace("/[^0-9]/", "",$config['upload']['maxSize'])* 1024 * 1024;
				$imageSize = $this->getSize($file['tmp_name']);
				if($maxSize < $imageSize){
					$errors[] = __da('Размер загружаемой картинки должен быть меньше чем ').$config['upload']['maxSize'].' MB';
				}
			}
		}
		return $errors;
	}

	/**
	 * Получить информацию о изображении.
	 * @param string $path Путь к файлу.
	 * @return boolean|array Массив данных идентичных результату вызова функции getimagesize(), или
	 * <b>false</b> если возникла ошибка или файл не является изображением.
	 */
	private function getImageSize($path) {
		if (!is_file($path)) {
			return false;
		}

		$output = getimagesize($path);
		$imageType = $output[2];

		if (!in_array($imageType, array(IMAGETYPE_JPEG, IMAGETYPE_PNG, IMAGETYPE_GIF))) {
			$this->error = 'Операция прервана. Формат файла (' . $path . ') не распознан как допустимый формат изображения.';
			return false;
		}

		return $output;
	}

	public function getCropPrefix(){
		return isset($this->settings['crop']['prefix']) ? $this->settings['crop']['prefix'] : 'crop_';
	}

	public function replaceExtensionToPng($file){
		$extension = mb_strtolower(pathinfo($file, PATHINFO_EXTENSION));
		return str_replace($extension,'png',$file);
	}

	public function createGifAnimation($images = []){
		if(!empty($images)){
			$i = 1;
			foreach ($images as $id => $image_dir){
//				header ('Content-type:image/gif');
				$text = "Slider ".$i;

// Open the first source image and add the text.
				if(mb_strtolower(pathinfo($image_dir, PATHINFO_EXTENSION)) == 'png'){
					$image = imagecreatefrompng($image_dir);
				}else{
					$image = imagecreatefromjpeg($image_dir);
				}
				$text_color = imagecolorallocate($image, 200, 200, 200);
				imagestring($image, 5, 5, 5,  $text, $text_color);

// Generate GIF from the $image
// We want to put the binary GIF data into an array to be used later,
//  so we use the output buffer.
				ob_start();
				imagegif($image);
				$frames[]=ob_get_contents();
				$framed[]=180;

// Delay in the animation.
				ob_end_clean();
				$i++;
			}
			$gif = new \App\Utility\Upload\GIFEncoder($frames,$framed,0,2,0,0,0,'bin');
			$filename = '';
			$fp = fopen(IMAGE_PATH.'/News/gallery/'.implode('-',array_keys($images)).'.gif', 'w');
			fwrite($fp, $gif->GetAnimation());
			fclose($fp);
			return $filename;
		}
	}
}