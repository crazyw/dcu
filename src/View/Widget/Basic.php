<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         3.0.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\View\Widget;

use Cake\View\Form\ContextInterface;
use Cake\View\Widget\WidgetInterface;

/**
 * Basic input class.
 *
 * This input class can be used to render basic simple
 * input elements like hidden, text, email, tel and other
 * types.
 */
class Basic implements WidgetInterface
{

    /**
     * StringTemplate instance.
     *
     * @var \Cake\View\StringTemplate
     */
    protected $_templates;

    /**
     * Constructor.
     *
     * @param \Cake\View\StringTemplate $templates Templates list.
     */
    public function __construct($templates)
    {
        $this->_templates = $templates;
    }

    /**
     * Render a text widget or other simple widget like email/tel/number.
     *
     * This method accepts a number of keys:
     *
     * - `name` The name attribute.
     * - `val` The value attribute.
     * - `escape` Set to false to disable escaping on all attributes.
     *
     * Any other keys provided in $data will be converted into HTML attributes.
     *
     * @param array $data The data to build an input with.
     * @param \Cake\View\Form\ContextInterface $context The current form context.
     * @return string
     */
    public function render(array $data, ContextInterface $context)
    {
        $data += [
            'name' => '',
            'val' => null,
            'type' => 'text',
            'escape' => true,
            'templateVars' => []
        ];
        $data['value'] = $data['val'];
        unset($data['val']);

        if($data['name'] == 'title'){
            $data['templateVars']['inputclass'] = isset($data['templateVars']['inputclass']) ? $data['templateVars']['inputclass'].' title-input' : 'title-input';
        }
        //set initial variable
        $value = $data['value'];
        $templateVars = $data['templateVars'];
        $name = $data['name'];
        if(get_class($context) !=  'Cake\View\Form\NullContext'){
            $entity = $context->entity();
        }

        //password exception
        if($data['name'] == 'password'){
            $data['value'] = '';
        }

        // template for language without model
        if(isset($data['lang']) && $data['lang'] === true){
            $data['required'] = false;
            $languages = [];
            $data['templateVars']['text'] = isset($templateVars['label']) ? $templateVars['label'] : '';
            foreach (lang() as $language) {
                if(!$language->admin) continue;
                if(!isset($data['prefix'])){
                    $data['value'] = $entity->translation($language->code)->{$name};
                    $data['id'] = 'translations-'.$language->code.'-'.$name;
                    $data['name'] = '_translations['.$language->code.']['.$name.']';
                }

                if(isset($data['prefix'])){
                    $data['value'] = $value[$data['prefix'].$language->code];
                    $data['name'] = str_replace('{LANG}', $language->code,$name);
                }
                $data['templateVars']['lang'] = $language->title;
                $data['data-lang'] = $language->id;
                $data['templateVars']['class'] = isset($templateVars['class']) ? $templateVars['class'].' lang-group lang-'.$language->code : ' lang-group lang-'.$language->code ;

                //mark field if in slug list from config
//                if(in_array($name,SlugListField()) || $name == 'slug'){
//                    $data['templateVars']['class'] = $data['templateVars']['class'].' sluggable';
//                    $data['data-field-origin'] = $name;
//                }

                if($name == 'slug'){
                    if(isset($entity->slug[$language->code])) {
                        $data['value'] = $entity->slug[$language->code];
                        if (!$entity->id) {
                            $data['templateVars']['slug'] = '<input type="checkbox" class="slug-checker" name="custom_slug[' . $language->id . ']">';
                            $data['disabled'] = 'disabled';
                        } else {
                            $data['templateVars']['slug'] = '<input type="checkbox" class="slug-checker" checked name="custom_slug[' . $language->id . ']">';
                        }
                    }
                }
                $languages[] = $this->_templates->format('language', [
                    'name' => $data['name'],
                    'type' => $data['type'],
                    'label'=>false,
                    'templateVars' => $data['templateVars'],
                    'attrs' => $this->_templates->formatAttributes(
                        $data,
                        ['name', 'type']
                    ),
                ]);
            }
            return $languages;
        }

        //template for datetimepicker
        if(in_array($data['name'],dev_conf('Config.dateFields'))){
            $data['value'] = strtotime($entity->{$data['name']}) ? date('Y-m-d',strtotime($entity->{$data['name']})) : date('Y-m-d');
            $data['templateVars']['text'] = isset($templateVars['label']) ? $templateVars['label'] : '';
            $data['templateVars']['convert_data'] = isset($entity->{$data['name']}) ? toNormalDate(date('Y-m-d', strtotime($entity->{$data['name']})),'rom',false) : toNormalDate(date('Y-m-d', time()),'rom',false);
            return $this->_templates->format('datetimepicker', [
                'name' => $data['name'],
                'type' => $data['type'],
                'templateVars' => $data['templateVars'],
                'attrs' => $this->_templates->formatAttributes(
                    $data,
                    ['name', 'type']
                ),
            ]);
        }

        //template for simple image
        if(in_array($data['name'],dev_conf('Config.singleImageFields'))){
            $template = 'single_image';
            if($entity->{$data['name']}){
                $template = 'single_image_with_image';
                $image_config = dev_conf('Config.singleImage.'.MODEL_NAME.'.'.$data['name']);
                $data['templateVars']['image_name'] = $entity->getImageItem($data['name']);
            }
            $data['templateVars']['text'] = isset($templateVars['label']) ? $templateVars['label'] : '';
            return $this->_templates->format($template, [
                'random'=>$entity->isNew() ? '<input type="hidden" name="random['.$data['name'].']" value="'.uniqid().'"/>' : '',
                'name' => $data['name'],
                'type' => $data['type'],
                'templateVars' => $data['templateVars'],
                'attrs' => $this->_templates->formatAttributes(
                    $data,
                    ['name', 'type']
                ),
            ]);
        }

        return $this->_templates->format('input', [
            'name' => $data['name'],
            'type' => $data['type'],
            'templateVars' => $data['templateVars'],
            'attrs' => $this->_templates->formatAttributes(
                $data,
                ['name', 'type']
            ),
        ]);
    }

    /**
     * {@inheritDoc}
     */
    public function secureFields(array $data)
    {
        if (!isset($data['name']) || $data['name'] === '') {
            return [];
        }

        return [$data['name']];
    }
}
