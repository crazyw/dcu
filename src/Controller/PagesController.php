<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Database\Expression\QueryExpression;


/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 * amigo2014office
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Csrf');
    }

    public function display()
    {
		$this->render('home');
    }

    public function lookbook()
    {
        $this->render('lookbook');
    }

    public function about()
    {
        $this->render('about');
    }


    public function wholesale()
    {
        $this->render('wholesale');
    }

    public function contacts()
    {
        $this->render('contact');
    }
    public function view($slug)
    {
       $this->getSetPage($slug);

    }
    public function contactForm()
    {
        $this->loadModel('Contacts');
        $request = $this->Contacts->newEntity($this->request->data);
        $out['success'] = false;
        if ($this->request->is('ajax')) {
            $out['success'] = false;
            $patch = $this->Contacts->patchEntity($request, $this->request->data);
            if(!empty($patch->errors())){
                $msg = $this->getValidationError($patch->errors());
                $out['errors'] = $msg;
            }else{
                $request->created = date('Y-m-d H:i:s');
                $save = $this->Contacts->save($request);
                if($save) {
                    $email = new \Cake\Mailer\Email();

                    $email->from(dev_conf('Settings.Notification.from_email'));
                    $email->to(dev_conf('Settings.Notification.to_email'));
                    $subject = 'Messaga from Contact us section - '.date('Y-m-d');
                    $email->subject($subject);
                    $email->viewVars(['request' => $save]);
                    $email->template('contact');
                    $email->setEmailFormat('html');
                    $email->send();
                    $out['success'] = true;
                }
            }
        }
        $this->response->type('json');
        $this->response->body(json_encode($out));
        $this->response->statusCode(200);
        $this->response->send();
        $this->response->stop();
    }
}
