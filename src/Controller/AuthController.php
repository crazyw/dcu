<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class AuthController extends AppController
{

	public function initialize()
	{
		parent::initialize();

		$this->loadComponent('RequestHandler');
		$this->loadComponent('Flash');
		$this->loadComponent('Cookie');

		$this->loadComponent('Auth',[
			'storage' => [
				'className' => 'Session',
				'key' => 'Auth.Front',
			],
			'loginRedirect' => [
				'controller' => 'Pages',
				'action' => 'display',
//				'language' => LANG
			],
			'logoutRedirect' => [
				'controller' => 'Pages',
				'action' => 'display',
//				'language' => LANG
			],
		]);

		$this->Auth->setConfig('authenticate', [
			'Basic' => ['userModel' => 'Members'],
			'Form' => ['userModel' => 'Members']
		]);

		$this->Auth->allow(['login','signup','confirmation','changePassword','forgotPassword','fb']);
		$this->viewBuilder()->setLayout('ajax');
	}

	public function signup()
	{
		if($this->Auth->user()){
			//@todo @amigo Redirect to home page???
		}
		$this->autoRender = false;
		$out['success'] = false;

		if ($this->request->is(['post', 'put'])) {
			$data = $this->request->data;
			$this->loadModel('Members');
			$entity = $this->Members->newEntity($data);
			$patch = $this->Members->patchEntity($entity, $data);
			if(!empty($patch->errors())){
				$msg = $this->getValidationError($patch->errors());
				$msg[] = __ds('Completati campurile marcate cu rosu');
				$out['errors'] = $msg;
			}else{
				$entity->status = 'inactive';
				$email_code = md5(uniqid().time());
				$entity->email_code = $email_code;
				$entity->email_send = date("Y-m-d H:i:s");
				$save = $this->Members->save($entity);
				if($save){
					$link = \Cake\Routing\Router::url([
						'controller' => $this->request->getParam('controller'),
						'action' => 'confirmation',
						'language'=>LANG,
						'code'=>$email_code],true
					);

//					 Send email for activation account
					$email = new \Cake\Mailer\Email();
					$email->to($entity->email,$entity->first_name.' '.$entity->last_name);
					$email->subject('Подтверждение email для сайта cinehub');
					$email->from('info@cinehub.md','Cinehub');
					$email->viewVars(['link' => $link]);
					$email->template('new_member');
					$email->setEmailFormat('html');
					$email->send();

					$out['success'] = true;
					$out['url'] = 'notification';
					$out['message'] = __ds('Va multumim pentru înregistrare. Verificați emailul și accesati linkul pentru a activa contul.');
//					$this->Auth->setUser($entity);
				}
			}
		}
		echo json_encode($out);
		die();
	}

	public function fb(){
		$out = [];
		$out['success'] = false;
		$this->loadModel('Members');
		$fbUserData = isset($this->request->data['fbUserData']) ? $this->request->data['fbUserData'] : false;
		$image_conf = dev_conf('Config.singleImage.Members.avatar.crop');
		$this->loadComponent('AmigoFile');

		if(!$fbUserData || !isset($fbUserData['id']) || (isset($fbUserData['id']) && strlen(trim($fbUserData['id'])) < 5)){
			$out['error'] = 'Error at Facebook login';
		}

		$fb = new \Facebook\Facebook([
			'app_id' => dev_conf('Config.Facebook.app_id'),
			'app_secret' => dev_conf('Config.Facebook.secret_key'),
			'default_graph_version' => 'v2.9',
		]);

		$helper = $fb->getJavaScriptHelper();
//		$helper = $fb->getCanvasHelper();
//		$permissions = ['email']; // optionnal

		//get $accessToken
		try {
			if($this->request->session()->read('facebook_access_token')){
				$accessToken = $this->request->session()->read('facebook_access_token');
			}else{
				$accessToken = $helper->getAccessToken();
			}
		} catch(\Facebook\Exceptions\FacebookResponseException $e) {
			$out['error'] =  'Graph returned an error: ' . $e->getMessage();
		} catch(\Facebook\Exceptions\FacebookSDKException $e) {
			$out['error'] =  'Facebook SDK returned an error: ' . $e->getMessage();
		}

		if (! isset($accessToken)) {
			$out['error'] =  'No cookie set or no OAuth data could be obtained from cookie.';
		}
		if(isset($out['error'])){
			echo json_encode($out);
			exit();
		}

		//save Token in session
		if($this->request->session()->read('facebook_access_token')){
			$fb->setDefaultAccessToken($this->request->session()->read('facebook_access_token'));
		}else{
			$this->request->session()->write('facebook_access_token',(string) $accessToken);
			$oAuth2Client = $fb->getOAuth2Client();
			$longLivedAccessToken = $oAuth2Client->getLongLivedAccessToken($this->request->session()->read('facebook_access_token'));
			$_SESSION['facebook_access_token'] = (string) $longLivedAccessToken;
			$fb->setDefaultAccessToken($this->request->session()->read('facebook_access_token'));
		}

		// validating the access token
		try {
			$request = $fb->get('/me');
		} catch(\Facebook\Exceptions\FacebookResponseException $e) {
			if ($e->getCode() == 190) {
				$this->request->session()->delete('facebook_access_token');
				exit;
			}
		} catch(\Facebook\Exceptions\FacebookSDKException $e) {
			echo 'Facebook SDK returned an error: ' . $e->getMessage();
			exit;
		}

		// getting profile picture of the user
		try {
			$requestPicture = $fb->get('/me/picture?redirect=false&height=600'); //getting user picture
			$requestProfile = $fb->get('/me?fields=email,first_name,last_name,name'); // getting basic info
			$picture = $requestPicture->getGraphUser();
			$profile = $requestProfile->getGraphUser();
		} catch(\Facebook\Exceptions\FacebookResponseException $e) {
			echo 'Graph returned an error: ' . $e->getMessage();
			exit;
		} catch(\Facebook\Exceptions\FacebookSDKException $e) {
			echo 'Facebook SDK returned an error: ' . $e->getMessage();
			exit;
		}
		//save member
		$member = $this->Members->find()->where(['fb_id' => $profile->getId()])->first();

		if(!$member){
			$partName = explode(' ',$profile->getName());

			$member = $this->Members->newEntity();
			if($profile->getEmail()) {
				$memberEmail = $this->Members->find()->where(['email' => $profile->getEmail()])->first();
				if($memberEmail){
					$member = $memberEmail;
				}else{
					$member->email = $profile->getEmail();
				}
			}else{
				$member->email = null;
			}

			$member->first_name = $profile->getFirstName() ? $profile->getFirstName() : (isset($partName[0]) ? $partName[0] : '' );
			$member->last_name = $profile->getLastName() ? $profile->getLastName() : (isset($partName[1]) ? $partName[1] : '' );
			$member->fb_id = $profile->getId();
			$member->status = 'pending';
			if($this->Members->save($member)){
				$out['success'] = true;
				if($picture->getField('url')) {
					$path = IMAGE_PATH . 'members' . DS . $member->id;
					$source_image = $picture->getField('url');
					// create thumbnails
					$createDest = new \Cake\Filesystem\Folder();
					if ($createDest->create($path, 0777)) {
						$image_file = $path . DS . 'avatar.jpg';
						if (copy($source_image, $image_file)) {
							$source_image = $image_file;
							if (isset($image_conf['thumbs'])) {
								foreach ($image_conf['thumbs'] as $dir => $thumb) {
									$destination = $path . DS . $dir;
									$createDest = new \Cake\Filesystem\Folder();
									if ($createDest->create($destination, 0777)) {
//										if (!empty($thumb['width']) && empty($thumb['height'])) {
//											$this->AmigoFile->resizeByWidth($source_image, $thumb['width'], $destination);
//										} elseif (empty($thumb['width']) && !empty($thumb['height'])) {
//											$this->AmigoFile->resizeByHeight($source_image, $thumb['height'], $destination);
//										} elseif (!empty($thumb['width']) && !empty($thumb['height'])) {
											$this->AmigoFile->smartCrop($source_image, $thumb['width'], $thumb['width'], $destination);
//										}
									}
								}
								$member->avatar = 'avatar.jpg';
								$this->Members->save($member);
							}
						}
					}
				}
			}
		}
		$this->Auth->setUser($member);

		$this->response->body(json_encode($out));
		$this->response->statusCode(200);
		$this->response->send();
		$this->response->stop();
	}

	public function confirmation(){
		$query = $this->request->query;
		if(isset($query['code']) && strlen(trim($query['code'])) > 5){
			$this->loadModel('Members');
			$entity = $this->Members->find()->where(['email_code'=>$query['code']])->first();
			if($entity){
				$entity->status = 'pending';
				$entity->email_code = '';
				$entity->email_send = null;
				if($this->Members->save($entity)){
					$this->Auth->setUser($entity);
					return $this->redirect(['controller'=>'Profile','action'=>'edit','language'=>LANG]);
				}
			}
		}
		return $this->redirect(['controller'=>'Pages','action'=>'display','language'=>LANG]);
	}

	public function forgotPassword()
	{
//		$this->autoRender = false;
		$out['success'] = false;
		$this->response->type('json');
		if ($this->request->is(['post', 'put'])) {
			$data = $this->request->data;
			$this->loadModel('Members');

			$entity = $this->Members->find()->where(['email'=>$data['email'],'status !='=>'inactive'])->first();
			if(!isset($data['terms']) || (isset($data['terms']) && $data['terms'] != 'on')){
				$out['errors']['terms'] = ' ';
			}
			if(!$entity){
				$out['errors']['email'] = 'Please check your entries and try again.';
			}
			if(empty($out['errors'])){
				$email_code = md5(uniqid() . time());
				$entity->email_code = $email_code;
				$entity->email_send = date("Y-m-d H:i:s");
				$save = $this->Members->save($entity);
				if ($save) {
					$link = \Cake\Routing\Router::url([
						'controller' => $this->request->getParam('controller'),
						'action'     => 'changePassword',
						'language'   => LANG,
						'code'       => $email_code], true
					);

					// Send email for change password
					$email = new \Cake\Mailer\Email();
					$email->to($entity->email, $entity->first_name . ' ' . $entity->last_name);
					$email->subject('Восстановление пароля для сайта cinehub');
					$email->from('info@cinehub.md', 'Cinehub');
					$email->viewVars(['link' => $link]);
					$email->template('forgot_password');
					$email->setEmailFormat('html');
					$email->send();

					$out['url'] = 'notification';
					$out['message'] = __ds('Verificați emailul și accesati linkul pentru a recuperare contul.');
//					$out['url'] = false;
					$out['success'] = true;
				}else {
					$out['errors']['email'] = 'Please check your entries and try again.';
				}
			}
		}
		$this->response->body(json_encode($out));
		$this->response->statusCode(200);
		$this->response->send();
		$this->response->stop();
	}

	public function changePassword()
	{
		if($this->authUser){
			return $this->redirect(['controller'=>'Pages','action'=>'display','language'=>LANG]);
		}
		
		$this->viewBuilder()->layout('default');
		$query = $this->request->query;
		if(isset($query['code']) && strlen(trim($query['code'])) > 5){
			$this->loadModel('Members');
			$entity = $this->Members->find()->where(['email_code'=>$query['code']])->first();
			if($entity){
				if ($this->request->is(['post', 'put'])) {
					$out['success'] = false;
					$password = isset($this->request->data['password']) ? $this->request->data['password'] : '';
					$password_repeat = isset($this->request->data['password_repeat']) ? $this->request->data['password_repeat'] : '';
					if($password && $password_repeat){
						if(strlen(trim($password)) < 6){
							$out['errors']['password'] = 'Parola trebuie să fie mai mare de 6 caractere.';
						}
						if($password != $password_repeat){
							$out['errors']['password_repeat'] = 'Parolele trebuie să se potrivească.';
						}
					}else{
						$out['errors'] = ['password' => 'Please check your entries and try again.'];
					}
					if(empty($out['errors'])){
						$entity->email_code = '';
						$entity->email_send = null;
						$entity->password = $password;
						if($this->Members->save($entity)){
							$this->Auth->setUser($entity);
							$out['success'] = true;
							$out['url'] = \Cake\Routing\Router::url([
											'controller' => 'Profile',
											'action'     => 'edit',
											'language'   => LANG
										  ], true
							);
						}
					}
					$this->response->type('json');
					$this->response->body(json_encode($out));
					$this->response->statusCode(200);
					$this->response->send();
					$this->response->stop();
				}
			}else{
				return $this->redirect(['controller'=>'Pages','action'=>'display','language'=>LANG]);
			}
		}else{
			return $this->redirect(['controller'=>'Pages','action'=>'display','language'=>LANG]);
		}

	}


	public function login()
	{
		if($this->Auth->user()){
			//@todo @amigo Redirect to home page???
		}
		$out['success'] = false;
		$out['errors'] = ['email'=>'Please check your entries and try again.','password'=>''];
		if ($this->request->is(['post', 'put'])) {
			$hasher = new \Cake\Auth\DefaultPasswordHasher();
			$data = $this->request->data;
			$this->loadModel('Members');
			$entity = $this->Members->find()->where(['email'=>$data['email'],'email IS NOT' => 'NULL','email !=' => ''])->first();
			if($entity){
				if ($entity->password == '' && $entity->fb_id) {
					$out['errors'] = ['email' => 'Please login throw facebook and set password in your profile.', 'password' => ''];
				}else {
					if ($hasher->check($data['password'], $entity->password)) {
						if ($entity->status == 'inactive') {
							$out['errors'] = ['email' => 'Please check your email and confirm email.', 'password' => ''];
						} else {
							$this->Auth->setUser($entity);
							$out['success'] = true;
							$out['url'] = '';
						}
					}
				}
//					$out['url'] = \Cake\Routing\Router::url([
//						'controller' => $this->request->getParam('controller'),
//						'language'=>LANG,
//						'action' => 'index']
//					);
			}
		}
		echo json_encode($out);
		die();
	}

	public function logout()
	{
		if($this->Auth->user()){
			//@todo @amigo Redirect to home page???
		}
		return $this->redirect($this->Auth->logout());
	}
}
