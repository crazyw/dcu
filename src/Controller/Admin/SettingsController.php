<?php
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;
use Cake\I18n\I18n;
use Cake\Core\Configure;
/**
 * Settings Controller
 *
 * @property \App\Model\Table\SettingsTable $Settings
 */
class SettingsController extends AppController
{
    public function deleteFavicon()
    {
        $this->response->type('json');

        $deleteDir = new \Cake\Filesystem\Folder(WWW_ROOT.'img/favicons/');
        if ($deleteDir->delete()) {
            $this->response->body(json_encode(['success' => true]));
            $this->response->statusCode(200);
        }else {
            $this->response->statusCode(403);
        }

        $this->response->send();
        $this->response->stop();
    }
}
