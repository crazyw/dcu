<?php
namespace App\Controller\Admin;

use Cake\Network\Exception\NotFoundException;

class EndorsementsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
	public function index() {
		parent::index();
		$this->set('title_for_layout', 'Endorsements');
		$this->loadModel('Endorsements');
		$this->paginate['order']['Endorsements.position'] = 'asc';
		$this->paginate['order']['Endorsements.id'] = 'desc';
        $this->paginate['limit'] = 100;
		$endorsements = $this->paginate($this->Endorsements);
		$this->set(compact('endorsements'));
	}

	public function add() {
		$this->set('title_for_layout', 'Endorsements : Add');
		$this->loadModel('Endorsements');
		$page = $this->Endorsements->newEntity($this->request->data);

		$this->set(compact('page'));
		$this->formValidation(false);
	}

	public function edit($id = NULL) {
		$this->loadModel('Endorsements');
		$page = $this->Endorsements->find('translations')->where(['Endorsements.id'=>$id])->first();
		$this->set('title_for_layout', 'Endorsements : '.$page->title);
		if (empty($page)) {
			throw new NotFoundException('Could not find that page.');
		} else {
			$this->set(compact('page'));
		}
		$this->formValidation(false);
	}
}
