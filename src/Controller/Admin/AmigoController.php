<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller\Admin;

use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\Network\Exception\UnauthorizedException;
use App\Controller\CommonController;
use Cake\Validation\Validator;

/**
 * Amigo Controller
 *
 * Add CMS methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AmigoController extends CommonController
{

    /**
     * Initialization hook method.
     *
     * Use this method to add CMS Amigo code
     *
     *
     * @return void
     */
    public function initialize() {
        parent::initialize();


        //load components
        $this->loadComponent('AmigoFile');
		$this->loadComponent('RequestHandler');
		$this->loadComponent('Auth');
		$this->loadComponent('Flash');
		$this->loadComponent('UserSettings');

		//set pagination limit in index action
		if(strtolower($this->request->getParam('action')) == 'index') {
			$this->paginate['limit'] = isset($this->request->query['per-page']) ? $this->UserSettings->set('per_page', (int)$this->request->query['per-page']) : $this->UserSettings->get('per_page');
			$this->set('per_page', $this->paginate['limit']);
		}

		//current auth user in admin panel
		$current_user = $this->Auth->user();

        $superUser = false;
        if ($current_user) {
            // avoid authorization if user is amigo or admin
            if ( in_array($current_user['id'], [AMIGO_USER_ID, ADMIN_USER_ID]) || $current_user['role_id'] == 2) {
                $superUser = true;
            } else {
                $this->isAuthorized($current_user['role_id']);
            }
        }

        // Set default language for admin
        \Cake\I18n\I18n::locale(dev_conf('Languages')[conf('Language.admin')]['code']);
        // load model global
        $this->loadModel('Pages');
        $this->loadModel('Modules');

        $this->set(compact('current_user','superUser'));

        //Layout
        $this->viewBuilder()->layout('admin');
    }

    protected function isAuthorized($roleId){
        $this->loadModel('Permissions');
        $permissions = $this->Permissions->find('list',[
            'keyField' => 'actions_module.action.name',
            'valueField' => 'actions_module.action.id',
            'groupField' => 'actions_module.module.name',
        ])->contain([
            'ActionsModules.Modules',
            'ActionsModules.Actions'
        ])->where([
            'Permissions.role_id' => $roleId,
            'Permissions.allow' => true,
        ])->toArray();

        $controllerName = $this->request->getParam('controller');
        $actionName = $this->request->getParam('action');

        $isNotAuthorized = ! isset($permissions[$controllerName][$actionName]);

        if ($isNotAuthorized) {
            if(count($permissions)){
                $this->redirect(\Cake\Routing\Router::url([
                    'controller' => key($permissions),
                    'action' => 'index'],true
                ));
            }else{
                throw new UnauthorizedException('admin-401'); // admin-401 is used to change template
            }
        }

        $this->set(compact('permissions'));
    }

    public function dashboard()
    {

		$this->_userLog = \Cake\ORM\TableRegistry::get('UserLog');
		$last_activity = $this->_userLog->find()->where(['action'=>'formValidation'])->contain(['Users'])->order(['UserLog.id'=>'DESC'])->limit(20)->all();
		$this->set(compact('last_activity'));
		$this->render('/Admin/Element/dashboard');
    }

    /**
     * General index for admin listing
     */
    public function index() {
        $this->request->session()->write('Back.' . MODEL_NAME, $this->request->here());
    }

    public function paginationSearch(){
		$model = $this->{MODEL_NAME};
		if(!empty($this->request->query)){
			$query = $this->request->query;
			if(isset($query['sort'])){
				unset($query['sort']);
			}
			if(isset($query['direction'])){
				unset($query['direction']);
			}
			if(isset($query['search'])){
				unset($query['search']);
			}
			if(!empty($query)){
				$selectSearch = [];
				foreach ($query as $k => $v){
					$selectSearch[str_replace('__','.',$k)] = $v;
				}
				$model = $this->{MODEL_NAME}->find()->where($selectSearch);
			}
		}
		${CONTROLLER_NAME} = $this->paginate($model);
		$this->set(CONTROLLER_NAME,${CONTROLLER_NAME});
	}

    /**
     * General add for admin items
     */
    public function add() {
    }

    /**
     * @param null $id
     * General edit for admin items
     */
    public function edit($id = NULL) {
    }

    public function formValidation($json = true,$other_data = array()){
        $this->autoRender = false;
        $out = [];
        if ($this->request->is(['post', 'put'])) {
            $out['success'] = false;
            $data = $this->request->data;
            $entity = (int)$data['id'] ? $this->{MODEL_NAME}->get($data['id']) : $this->{MODEL_NAME}->newEntity($data);
            $patch = $this->{MODEL_NAME}->patchEntity($entity, $data, isset($other_data['patchEntity']) ? $other_data['patchEntity'] : []);
            if(!empty($patch->errors())){
                $msg = $this->getValidationError($patch->errors());
                $out['errors'] = $msg;
            }else{
                $save = $this->{MODEL_NAME}->save($entity);
                if($save){
                    Cache::clear(false,'site');
                    $out['redirect_url'] = $this->request->session()->check('Back.' . MODEL_NAME) ?
                                                $this->request->session()->read('Back.' . MODEL_NAME) :
                                                '/admin/'.MODEL_NAME;
                    $out['success'] = true;
                    $out['url'] = \Cake\Routing\Router::url([
                        'controller' => $this->request->getParam('controller'),
                        'action' => 'edit',
                        'id' => $save->id],true
                    );
                    $out['id'] = $save->id;
                }
            }
        }

		if(!empty($out) && !$json){
			if($out['success']){
				$this->Flash->set('The '.CONTROLLER_NAME.' has been saved.',
					[
						'key' => 'admin',
						'element' => 'admin',
						'params' => [
							'class' => 'success'
						]]
				);
				if(!$json) {
					$this->formRedirect();
				}
			}else{
				$this->Flash->set('Please fix error(s).',
					[
						'key' => 'admin',
						'element' => 'admin',
						'params' => [
							'class' => 'error'
						]]
				);
			}
		}

        if($json){
            echo json_encode($out);
            exit();
        }

        $this->render('form');
    }

    public function footerAction(){
        $this->autoRender = false;
        $actions = [1=>'delete',2=>'publish'];
        if(isset($this->request->query['action']) && isset($this->request->query['checked']) && count($this->request->query['checked']) > 0){
            $action = $this->request->query['action'];
            $checked = $this->request->query['checked'];
            if(isset($actions[$action])){
                foreach ($checked as $id){
                    $entity = $this->{MODEL_NAME}->get($id);
                    if($entity) {
                        if($actions[$action] == 'delete') {
                            if ( isset($entity->module) && ! empty($entity->module) ) {
                                // @todo @amigo return something if user want to delete module
                            } else {
                                $this->{MODEL_NAME}->delete($entity);
                            }
                        }elseif($actions[$action] == 'publish'){
                            $entity->published = !$entity->published;
                            $save = $this->{MODEL_NAME}->save($entity);
                        }
                    }
                }
            }
        }
    }

    public function copy($id = null){

	}

    /**
     * @param null $id
     * @return \Cake\Network\Response|null
     * General function for delete items
     */
    public function delete($id = null)
    {
        $this->viewBuilder()->layout(false);
        $model = $this->{MODEL_NAME}->get($id);
        if ($this->{MODEL_NAME}->delete($model)) {
			$this->Flash->set('The '.CONTROLLER_NAME.' has been deleted.',
				[
					'key' => 'admin',
					'element' => 'admin',
					'params' => [
						'class' => 'success'
					]]
			);
        } else {
			$this->Flash->set('The '.CONTROLLER_NAME.' could not be deleted. Please, try again.',
				[
					'key' => 'admin',
					'element' => 'admin',
					'params' => [
						'class' => 'error'
					]]
			);
        }
        Cache::clear(false,'site');
        $this->render(false);
        $this->formRedirect();
    }

    /**
     * @param null $id
     * @return \Cake\Network\Response|null
     * General function for publish items
     */
    public function publish($id = null)
    {
        $item = $this->{MODEL_NAME}->get($id);
        $item->published = !$item->published;
        $this->{MODEL_NAME}->save($item);
        $this->viewBuilder()->layout(false);
        Cache::clear(false,'site');
        $this->render(false);
        $this->formRedirect();
    }

    /**
     * Update menu nested position
     */
    public function updateNested(){
        $out = ['success'=>false];
        $this->autoRender = false;
        $data = $this->request->data();
        $save = [];
        if(isset($data['ch']) && count($data['ch']) > 0){
            $ch = $data['ch'];
            $i = 0;
            foreach ($ch as $id => $parent_id){
                $menu = $this->{MODEL_NAME}->get($id);
                $menu->parent_id = $parent_id == 'null' ? NULL : (int)$parent_id;
                $menu->position = $i;
                $this->{MODEL_NAME}->save($menu);
                $i++;
            }
            $out = ['success'=>true];
        }
        echo json_encode($out);
        exit();
    }

    /**
     * @return \Cake\Network\Response|null
     * Redirect after save item
     */
    protected function formRedirect(){
        if ($this->request->session()->check('Back.' . MODEL_NAME)) {
            $redirect = $this->request->session()->read('Back.' . MODEL_NAME);
            $this->request->session()->delete('Back.' . MODEL_NAME);
            return $this->redirect($redirect);
        }else{
            return $this->redirect(['action' => 'index']);
        }
    }

    protected function getValidationError($errors){
        $msg = [];
        foreach ($errors as $name => $error){
            if($name == '_translations'){
                foreach ($error as $lang_name => $lang){
                    foreach ($lang as $field_name => $field) {
                        $msg['_translations[' . $lang_name . ']['.$field_name.']'] = array_values($field)[0];
                    }
                }
            }else{
                if(!is_array(array_values($error)[0])) {
                    $msg[$name] = array_values($error)[0];
                }
            }
        }
        return $msg;
    }

    public function uploadFile(){
		$this->autoRender = false;
		$output = array();

		//Нормальный вид
		$current_file_name = isset($this->request->query['name']) ? $this->request->query['name'] : 'name';
		$multiple = dev_conf('Config.'.MODEL_NAME.'.fileFields.'.$current_file_name.'.multiple') ? true : false;
		if($multiple) {
			$countFiles = count($_FILES['file_upload']['name'][$current_file_name]);
			for ($i = 0; $i < $countFiles; $i++) {
				$files[$i]['name'] = $_FILES['file_upload']['name'][$current_file_name][$i];
				$files[$i]['type'] = $_FILES['file_upload']['type'][$current_file_name][$i];
				$files[$i]['tmp_name'] = $_FILES['file_upload']['tmp_name'][$current_file_name][$i];
				$files[$i]['error'] = $_FILES['file_upload']['error'][$current_file_name][$i];
				$files[$i]['size'] = $_FILES['file_upload']['size'][$current_file_name][$i];
			}
		}else{
			foreach ($_FILES['file_upload'] as $key => $photo) {
				foreach ($photo as $prop => $value) {
					$files[$prop][$key] = $value;
				}
			}
			$files[0] = $files[$current_file_name];
			unset($files[$current_file_name]);
		}


		//Исключения для галереи
		$files_path = '';
		if($current_file_name === 'files'){
			$files_path = '/files';
		}
		// Данные из настроек
		$file_conf = dev_conf('Config.' . MODEL_NAME . '.fileFields.' . $current_file_name);

		foreach ($files as $file_key => $file_opt) {
			//Пути к папке абсолютные и для просмотра с браузера
			if (isset($this->request->data['id']) && $this->request->data['id'] && $current_file_name != 'files') {
				$path = FILE_PATH . strtolower(MODEL_NAME) . DS . $this->request->data['id'];
				$short_path = FILE_PATH_WEB . strtolower(MODEL_NAME) . DS . $this->request->data['id'];
			} else {
				$_POST['random'][$current_file_name] = uniqid();
				$path = FILE_PATH . strtolower(MODEL_NAME) . DS . $files_path . DS . FILE_PATH_TMP . DS . $_POST['random'][$current_file_name];
				$short_path = FILE_PATH_WEB . strtolower(MODEL_NAME) . $files_path . '/' . FILE_PATH_TMP . DS . $_POST['random'][$current_file_name];
				$output[$file_key]['random'] = $_POST['random'][$current_file_name];
			}


			//Проверка файл на конфигурации из настроек
			$fileErrors = $this->AmigoFile->getFileErrors($file_opt, $file_conf);
			if (!empty($fileErrors)) {
				$output[$file_key]['success'] = false;
				$output[$file_key]['error'] = $fileErrors[0];
				echo json_encode($output);
				die;
			}

			// Загрузка файла на сервер
			$filename = $this->AmigoFile->upload($file_opt, $path);
			if (empty($filename)) {
				$output[$file_key]['success'] = false;
				$output[$file_key]['error'] = $this->AmigoFile->error;
			} else {
				$output[$file_key]['success'] = true;
				$output[$file_key]['path'] = $short_path . '/';
				$output[$file_key]['filename'] = $filename;
				$output[$file_key]['uniqid'] = uniqid();
			}
		}
		echo json_encode($output);
		die;

	}


    public function upldateListPosition(){
		$this->autoRender = false;
		$data = $this->request->data;

		$success = $failure = 0;
		if (!empty($data['positions'])) {
			$i = 0;
			foreach ($_POST['positions'] as $id) {
				$menu = $this->{MODEL_NAME}->get($id);
				$menu->position = $i;
				$this->{MODEL_NAME}->save($menu) ? $success++ : $failure++;;
				$i++;
			}
            Cache::clear(false,'site');
			echo json_encode(array('success' => $success, 'failure' => $failure));
		}
		die;
	}

    public function deleteImage(){
		$this->autoRender = false;
		$data = $this->request->data;
		$prefix = '/';
		if($data['name'] == 'gallery'){
			$prefix = '/gallery/';
		}

		if($data['type'] == 'isset'){
			$folder = IMAGE_PATH.strtolower(MODEL_NAME).$prefix.$data['id'];
			$dir = new \Cake\Filesystem\Folder($folder);
            $dir->delete();
            if($data['name'] == 'gallery') {
                $this->loadModel('Images');
                $model = $this->Images->get($data['id']);
                $this->Images->delete($model);
            }else{
                $model = $this->{MODEL_NAME}->get($data['id']);
                $model->{$data['name']} = null;
                $this->{MODEL_NAME}->save($model);
            }
		}else{
			$folder = IMAGE_PATH.strtolower(MODEL_NAME).$prefix.IMAGE_PATH_TMP.'/'.$data['id'];
			$dir = new \Cake\Filesystem\Folder($folder);
			$dir->delete();

		}
		echo json_encode(['success'=>true]);
		die;
	}

	public function deleteFile(){
		$this->autoRender = false;
		$data = $this->request->data;
		$config = dev_conf('Config.'.MODEL_NAME.'.fileFields.'.$data['name']);

		if($config) {
			$prefix = '/';
			if($data['name'] == 'files'){
				$prefix = '/files';
			}
			if(is_numeric($data['id'])){
				$folder = FILE_PATH . strtolower(MODEL_NAME) . $prefix . DS . $data['id'];
				$dir = new \Cake\Filesystem\Folder($folder);
				if ($dir->delete()) {
					if($data['name'] == 'files'){
						$this->loadModel('Files');
						$model = $this->Files->get($data['id']);
						$this->Files->delete($model);
					}else{
						$model = $this->{MODEL_NAME}->get($data['id']);
						$model->{$data['name']} = '';
						$model->{$config['input']} = '';
						$this->{MODEL_NAME}->save($model);
					}
				}
			}else{
				$folder = FILE_PATH . strtolower(MODEL_NAME) . $prefix . DS . FILE_PATH_TMP . DS .  $data['id'];
				$dir = new \Cake\Filesystem\Folder($folder);
				$dir->delete();
			}
		}

		echo json_encode(['success'=>true,'name'=>$data['id']]);
		die;
	}

	public function search2ajax(){
		$this->autoRender = false;
		$data = $this->request->query;
		$out = [];
		$out['items'][] = ['id' => '', 'name' => 'All'];
		if(isset($data['model']) && isset($data['name']) && isset($data['q'])) {
			$search = $data['q'];

			$this->loadModel($data['model']);
			if(isset($data['find']) && $data['find']){
				$find = $this->{$data['model']}->find($data['find']);
			}else{
				$find = $this->{$data['model']}->find();
			}
			$ex = explode(',',$data['name']);
			foreach ($ex as $col){
				$where['OR'][$col.' LIKE'] = '%' . $search . '%';
			}
			$find = $find->where($where)->limit(10)->all();
			foreach ($find as $result) {
				$name = '';
				foreach ($ex as $col){
					$name .= $result->{$col}.' ';
				}
				$out['items'][] = ['id' => $result->id, 'name' => $name];
			}
			$out['total_count'] = count($find);
		}
		echo json_encode($out);
		die();
	}

	/**
	 * Generate Slug
	 */
	public function generateSlug(){
		$this->loadModel('Slugs');

		$out = [];
		$out['success'] = true;

		$target = $this->request->data['target'];
		$lang_id = $this->request->data['lang'];
		$slugFields = SlugListField();
		$slugFields[] = 'slug';
		$module_id = dev_conf('Modules')[MODEL_NAME]->id;
		if(!in_array($target,$slugFields)){
			$out['success'] = false;
			$this->response->body(json_encode($out));
			$this->response->statusCode(200);
			$this->response->send();
			$this->response->stop();
		}

		foreach (lang() as $lang) {
			$slugConverter = '';
			if($target != 'slug') {
				foreach (SlugListField() as $field) {
					$name = isset($this->request->data[$field]) ? $this->request->data[$field] : $this->request->data['_translations'][$lang->code][$field];
					$slugConverter .= $name . '-';
				}
				$slugConverter = getTranslit(\Cake\Utility\Inflector::slug($slugConverter, '-'));
			}else{
				$slugConverter = isset($this->request->data[$target]) ? $this->request->data[$target] : $this->request->data['_translations'][$lang->code][$target];
				$slugConverter = getTranslit(\Cake\Utility\Inflector::slug($slugConverter, '-'));
			}

			$slug = $this->Slugs->find()->where(['slug' => $slugConverter,'module_id'=>$module_id,'foreign_key !=' => $this->request->data['id']])->first();
			if ($slug) {
				//get next id for add, current id for edit
				if ($this->request->data['id']) {
					$item_id = $this->request->data['id'];
				} else {
					$conn = \Cake\Datasource\ConnectionManager::get('default');
					$stmt = $conn->execute("SHOW TABLE STATUS LIKE '" . strtolower(MODEL_NAME) . "'");
					$result = $stmt->fetchAll('assoc');
					$item_id = $result[0]['Auto_increment'];
				}
				$slugConverter = $slugConverter.'-'.$item_id;
//				$out['success'] = false;
//				$out['error'] = 'This slug is busy';
			}
			$out['slug_'.$lang->code] = $slugConverter;
		}
		$this->response->body(json_encode($out));
		$this->response->statusCode(200);
		$this->response->send();
		$this->response->stop();
	}

	protected function cloneFiles($from,$to){
		//images copy
		$source = IMAGE_PATH.strtolower(MODEL_NAME).DS.$from->id;
		$destination = IMAGE_PATH.strtolower(MODEL_NAME).DS.$to->id;
		$dir = new \Cake\Filesystem\Folder();
		$copy = $dir->copy([
			'to' => $destination,
			'from' => $source,
			'mode' => 0777,
		]);

		//file copy
		$source = FILE_PATH.strtolower(MODEL_NAME).DS.$from->id;
		$destination = FILE_PATH.strtolower(MODEL_NAME).DS.$to->id;
		$dir = new \Cake\Filesystem\Folder();
		$copy = $dir->copy([
			'to' => $destination,
			'from' => $source,
			'mode' => 0777,
		]);

		//gallery images
		if($from->images){
			foreach ($from->images as $image_key => $image){
				$source = IMAGE_PATH.strtolower(MODEL_NAME).DS.'gallery'.DS.$image->id;
				$destination = IMAGE_PATH.strtolower(MODEL_NAME).DS.'gallery'.DS.$to->images[$image_key]->id;
				$dir = new \Cake\Filesystem\Folder();
				$copy = $dir->copy([
					'to' => $destination,
					'from' => $source,
					'mode' => 0777,
				]);
			}
		}

		//files
		if($from->files){
			foreach ($from->files as $file_key => $file){
				$source = FILE_PATH.strtolower(MODEL_NAME).DS.'files'.DS.$file->id;
				$destination = FILE_PATH.strtolower(MODEL_NAME).DS.'files'.DS.$to->files[$file_key]->id;
				$dir = new \Cake\Filesystem\Folder();
				$copy = $dir->copy([
					'to' => $destination,
					'from' => $source,
					'mode' => 0777,
				]);
			}
		}
	}
}
