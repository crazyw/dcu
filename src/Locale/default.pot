# LANGUAGE translation of CakePHP Application
# Copyright YEAR NAME <EMAIL@ADDRESS>
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"POT-Creation-Date: 2017-03-22 11:38+0000\n"
"PO-Revision-Date: YYYY-mm-DD HH:MM+ZZZZ\n"
"Last-Translator: NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <EMAIL@ADDRESS>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: Controller/Admin/SettingsController.php:69;94
msgid "The setting has been saved."
msgstr ""

#: Controller/Admin/SettingsController.php:73;98
msgid "The setting could not be saved. Please, try again."
msgstr ""

#: Controller/Admin/SettingsController.php:116
msgid "The setting has been deleted."
msgstr ""

#: Controller/Admin/SettingsController.php:118
msgid "The setting could not be deleted. Please, try again."
msgstr ""

#: Template/Admin/Settings/add.ctp:8
#: Template/Admin/Settings/edit.ctp:8
#: Template/Admin/Users/index.ctp:8;22
msgid "Actions"
msgstr ""

#: Template/Admin/Settings/add.ctp:9
#: Template/Admin/Settings/edit.ctp:15
msgid "List Settings"
msgstr ""

#: Template/Admin/Settings/add.ctp:15
msgid "Add Setting"
msgstr ""

#: Template/Admin/Settings/add.ctp:21
#: Template/Admin/Settings/edit.ctp:27
msgid "Submit"
msgstr ""

#: Template/Admin/Settings/edit.ctp:10
#: Template/Admin/Users/index.ctp:36
msgid "Delete"
msgstr ""

#: Template/Admin/Settings/edit.ctp:12
#: Template/Admin/Users/index.ctp:36
msgid "Are you sure you want to delete # {0}?"
msgstr ""

#: Template/Admin/Settings/edit.ctp:21
msgid "Edit Setting"
msgstr ""

#: Template/Admin/Users/index.ctp:9
msgid "New User"
msgstr ""

#: Template/Admin/Users/index.ctp:13
msgid "Users"
msgstr ""

#: Template/Admin/Users/index.ctp:34
msgid "View"
msgstr ""

#: Template/Admin/Users/index.ctp:35
msgid "Edit"
msgstr ""

#: Template/Admin/Users/index.ctp:44
msgid "first"
msgstr ""

#: Template/Admin/Users/index.ctp:45
msgid "previous"
msgstr ""

#: Template/Admin/Users/index.ctp:47
msgid "next"
msgstr ""

#: Template/Admin/Users/index.ctp:48
msgid "last"
msgstr ""

#: Template/Admin/Users/index.ctp:50
msgid "Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total"
msgstr ""

#: Template/Layout/error.ctp:35
msgid "Error"
msgstr ""

#: Template/Layout/error.ctp:43
msgid "Back"
msgstr ""

