<?php
define('IMAGE_PATH',WWW_ROOT.'pic'.DS);
define('IMAGE_PATH_WEB','pic'.DS);
define('IMAGE_PATH_TMP','tmp');

define('FILE_PATH',WWW_ROOT.'files'.DS);
define('FILE_PATH_WEB','files'.DS);
define('FILE_PATH_TMP','tmp');

$uploadMaxFileSize = ini_get('upload_max_filesize');
$postMaxSize = ini_get('post_max_size');
$maxFileSize = 8;//intval($uploadMaxFileSize) < intval($postMaxSize) ? $uploadMaxFileSize : $postMaxSize;

define('MAX_FILESIZE', $maxFileSize);
// General settings
$config['Config']['baseUrl'] = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://':'http://').$_SERVER['HTTP_HOST'].'/';
$config['Config']['fullUrl'] = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://':'http://').$_SERVER['HTTP_HOST'].'/';

//password
$config['Config']['universalUser'] = 'info@amigo.md';


$config['Config']['menuCategory'] = [
    0 => 'Header menu'
];
$config['Config']['jpgQuality'] = 100;
$config['Config']['pngCompression'] = 0;
$config['Config']['menuPosition'] = [
	'-----',
	'Page',
	'External link'
];
$config['Config']['tehnicalPages'] = [
	   null => '-----',
      'Pages/display' => 'Home page',
      'Pages/contacts' => 'Contacts',
      'Pages/about' => 'About',
      'Pages/lookbook' => 'Lookbook',
      'Pages/wholesale' => 'Wholesale',
];


//Dates
$config['Config']['dateFields'] = ['created','modified','release_date','birthday'];
$config['Config']['dateFormat'] = 'd.m.Y';
$config['Config']['datePickerFormat'] = 'dd.mm.yy';

$config['Config']['fileModels'] = ['News'=>'News'];

$config['Config']['optionType'] = ['stars' => 'Rating','text' => 'Text','price' => 'Price','number' => 'Number','checkbox' => 'Checkbox'];

$optionTypeJson = [];
foreach ($config['Config']['optionType'] as $key => $item){
    $optionTypeJson[] = ['value' => $key,'label' => $item];
}
$config['Config']['optionTypeJson'] = $optionTypeJson;

//Users
$config['Config']['Users'] = [
	'searchFields' => ['full_name','username'],
];

//Translations
$config['Config']['Translations'] = [];

//Pages
$config['Config']['Pages'] = [
	'searchFields' => ['title'],
	'translate' => [
		'fields' => [
			'title',
			'content',
			'meta_description',
			'meta_title',
			'meta_keywords',
			'description'
		],
		'validator' => false
	]
];

//Menus
$config['Config']['Menus'] = [
    'translate' => [
        'fields' => [
            'title',
            'slogan',
            'content',
            'external_link'
        ],
    ]
];

//Faq settings
$config['Config']['Faq'] = [
    'translate' => [
        'fields' => [
            'title',
            'content'
        ],
    ],
];

$config['Config']['Lookbooks'] = [
    'translate' => [
        'fields' => [
            'title',
            'price',
            'content',
            'fabric'

        ],
    ],
];


//Faq settings
$config['Config']['Endorsements'] = [
    'translate' => [
        'fields' => [
            'name',
            'workplace',
            'content'
        ],
    ],
];

$config['Config']['Aboutpages'] = [
    'translate' => [
        'fields' => [
            'title',
            'content'


        ],
    ],
];


//Settings
$config['Config']['Settings'] = [];


// Сортировка по умолчанию
$config['Config']['sortableField'] = 'position';

// Тут настроики только по загрузке картинок
//single image and gallery. Gallery name is "gallery" in this settings

// В каких моделях подгружать галерею
$config['Config']['galeryModels'] = [];

$config['Config']['singleImageFields'] = ['image','icon','logo'];

$config['Config']['singleImage'] = [
    'Menus' => [
        'icon' => [
            'upload' => [
                'maxWidth' => 7000,
                'maxHeight' => 5000,
                'minWidth' => 200,
                'minHeight' => 520,
                'maxSize' => MAX_FILESIZE,
            ],
            'thumbs' =>[
                'lazy' => [
                    'width' => 96
                ],
                'medium' => [
                    'width' => 300
                ],
                'big' => [
                    'width' => 960,
                ]
            ],
            'preview' => [
                'folder' => 'medium',
                'width' => 'auto',
            ],
        ]
    ]
];

$config['Config']['singleImage'] = [
    'Lookbooks' => [
        'image' => [
            'upload' => [
                'maxWidth' => 7000,
                'maxHeight' => 5000,
                'minWidth' => 40,
                'minHeight' => 40,
                'maxSize' => MAX_FILESIZE,
            ],
            'preview' => [
                'folder' => false,
                'width' => 'auto',
            ],
        ]
    ],
    'Aboutpages' => [
        'image' => [
            'upload' => [
                'maxWidth' => 7000,
                'maxHeight' => 5000,
                'minWidth' => 40,
                'minHeight' => 40,
                'maxSize' => MAX_FILESIZE,
            ],
            'preview' => [
                'folder' => false,
                'width' => 'auto',
            ],
        ]
    ]


];


$config['Config']['favicons'] = [
	[
		'name' => 'favicon.ico',
		'width' => 16,
		'height' => 16,
	],
	[
		'name' => 'favicon-96x96.png',
		'width' => 96,
		'height' => 96,
	],
	[
		'name' => 'favicon-32x32.png',
		'width' => 96,
		'height' => 96,
	],
	[
		'name' => 'favicon-16x16.png',
		'width' => 16,
		'height' => 16,
	],
	[
		'name' => 'android-icon-36x36.png',
		'width' => 36,
		'height' => 36,
	],
	[
		'name' => 'android-icon-48x48.png',
		'width' => 48,
		'height' => 48,
	],
	[
		'name' => 'android-icon-72x72.png',
		'width' => 72,
		'height' => 72,
	],
	[
		'name' => 'android-icon-96x96.png',
		'width' => 96,
		'height' => 96,
	],
	[
		'name' => 'android-icon-144x144.png',
		'width' => 144,
		'height' => 144,
	],
	[
		'name' => 'android-icon-192x192.png',
		'width' => 192,
		'height' => 192,
	],
	[
		'name' => 'apple-icon-57x57.png',
		'width' => 57,
		'height' => 57,
		'rel'=>'apple-touch-icon',
	],
	[
		'name' => 'apple-icon-60x60.png',
		'width' => 57,
		'height' => 57,
	],
	[
		'name' => 'apple-icon-72x72.png',
		'width' => 72,
		'height' => 72,
	],
	[
		'name' => 'apple-icon-76x76.png',
		'width' => 76,
		'height' => 76,
	],
	[
		'name' => 'apple-icon-114x114.png',
		'width' => 114,
		'height' => 114,
	],
	[
		'name' => 'apple-icon-120x120.png',
		'width' => 120,
		'height' => 120,
	],
	[
		'name' => 'apple-icon-144x144.png',
		'width' => 144,
		'height' => 144,
	],
	[
		'name' => 'apple-icon-152x152.png',
		'width' => 152,
		'height' => 152,
	],
	[
		'name' => 'apple-icon-180x180.png',
		'width' => 180,
		'height' => 180,
	],
	[
		'name' => 'apple-icon-precomposed.png',
		'width' => 180,
		'height' => 180,
	],
	[
		'name' => 'apple-icon.png',
		'width' => 180,
		'height' => 180,
	],
	[
		'name' => 'ms-icon-70x70.png',
		'width' => 70,
		'height' => 70,
	],
	[
		'name' => 'ms-icon-144x144.png',
		'width' => 144,
		'height' => 144,
	],
	[
		'name' => 'ms-icon-150x150.png',
		'width' => 150,
		'height' => 150,
	],
	[
		'name' => 'ms-icon-310x310.png',
		'width' => 310,
		'height' => 310,
	]
];
