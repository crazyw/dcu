<?php
use Migrations\AbstractMigration;

class AddImagesTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
	public function up()
	{
		$this->query("CREATE TABLE `images` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`name` varchar(255) DEFAULT NULL,
`module_id` int(11) DEFAULT NULL,
`foreign_key` int(11) DEFAULT NULL,
`position` int(11) DEFAULT '0',
PRIMARY KEY (`id`),
KEY `images_item_id` (`foreign_key`),
KEY `module_id` (`module_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
	}

}
