<?php
use Migrations\AbstractMigration;

class PermisionTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
	public function up()
	{
		$this->query("CREATE TABLE `permissions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(11) unsigned NOT NULL,
  `action_module_id` int(11) unsigned NOT NULL,
  `allow` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `role_id_fk1_idx` (`role_id`),
  KEY `action_module_id_fk4_idx` (`action_module_id`),
  CONSTRAINT `action_module_id_fk4` FOREIGN KEY (`action_module_id`) REFERENCES `actions_modules` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `role_id_fk1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;");
	}
}
