<?php
use Migrations\AbstractMigration;

class UpdatesTable extends AbstractMigration
{
	public function up()
	{
		$this->query("CREATE TABLE `updates` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `version` varchar(255) DEFAULT NULL,
  `published` tinyint(1) DEFAULT '0',
  `last_update` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `is_new` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
CREATE TABLE `update_plugins` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `update_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `published` tinyint(1) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;");
	}
}
