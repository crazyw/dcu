<?php
use Cake\Core\Configure;
/**
 * Read admin settings from cache
 */
Configure::restore('Settings');
if(!Configure::check('Settings')){
    Configure::write('Settings',Cake\ORM\TableRegistry::get('Settings')->find('list',
            [
            'groupField'=>'col_group',
            'keyField'=>'col_key',
            'valueField'=>'value'
            ])->all()->toArray());
    Configure::store('Settings', 'default');
}


/**
 * Get translation from db and save in cache
 */

Cake\I18n\I18n::config('admin', function ($name, $locale) {
   $package = new Aura\Intl\Package('default');
   $message = Cake\ORM\TableRegistry::get('Translations')
    ->find('list',['keyField'=>'msgid','valueField'=>'content'])
    ->where(['domain'=>$name])
    ->contain(['Languages'])
    ->all()->toArray();
   $package->setMessages($message);
   $package->setFallback('default');
   return $package;
});

Cake\I18n\I18n::config('site', function ($name, $locale) {
    $package = new Aura\Intl\Package('default');
    $message = Cake\ORM\TableRegistry::get('Translations')
        ->find('list',['keyField'=>'msgid','valueField'=>'content'])
        ->where(['domain'=>$name])
        ->contain(['Languages'])
        ->all()->toArray();
    $package->setMessages($message);
    $package->setFallback('default');
    return $package;
});

/**
 * Get languages from db and save in cache
 */
Configure::restore('Languages');
if(!Configure::check('Languages')){
    $languageData = Cake\ORM\TableRegistry::get('Languages')->find()->all()->toArray();
    $languages = [];
    $languages_data = [];
    foreach ($languageData as $lang){
        $languages[$lang->id] = $lang;
        $languages_data[$lang->code] = $lang;
    }

    Configure::write('LanguagesData',$languages_data);
    Configure::store('LanguagesData', 'default');

    Configure::write('Languages',$languages);
    Configure::store('Languages', 'default');
}
foreach (\Cake\Core\Configure::read('LanguagesData') as $lang_code => $lang) {
    Cake\I18n\I18n::config('site', function ($name, $locale){
        $package = new Aura\Intl\Package('default');
        $message = Cake\ORM\TableRegistry::get('Translations')
            ->find('list', ['keyField' => 'msgid', 'valueField' => 'content'])
            ->where(['domain' => $name, 'language_id' => dev_conf('LanguagesData')[$locale]->id])
            ->contain(['Languages'])
            ->all()->toArray();
        $package->setMessages($message);
        $package->setFallback('default');
        return $package;
    });
}

Configure::restore('Modules');
if(!Configure::check('Modules')){
    $moduleData = Cake\ORM\TableRegistry::get('Modules')->find()->all()->toArray();
    $module = [];
    foreach ($moduleData as $mod){
        $module[$mod->name] = $mod;
    }
    Configure::write('Modules',$module);
    Configure::store('Modules', 'default');
}

if ( ! defined('AMIGO_USER_ID')) {
    define('AMIGO_USER_ID', 1); // id of user amigo (superuser)
}

if ( ! defined('ADMIN_USER_ID')) {
    define('ADMIN_USER_ID', 2); // id of user admin (superuser for client)
}
