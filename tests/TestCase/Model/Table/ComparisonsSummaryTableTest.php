<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ComparisonsSummaryTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ComparisonsSummaryTable Test Case
 */
class ComparisonsSummaryTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ComparisonsSummaryTable
     */
    public $ComparisonsSummary;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.comparisons_summary'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ComparisonsSummary') ? [] : ['className' => 'App\Model\Table\ComparisonsSummaryTable'];
        $this->ComparisonsSummary = TableRegistry::get('ComparisonsSummary', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ComparisonsSummary);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
