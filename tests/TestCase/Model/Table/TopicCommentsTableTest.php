<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TopicCommentsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TopicCommentsTable Test Case
 */
class TopicCommentsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TopicCommentsTable
     */
    public $TopicComments;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.topic_comments',
        'app.topics',
        'app.members',
        'app.languages',
        'app.translations',
        'app.specializations',
        'app.specializations_title_translation',
        'app.i18n',
        'app.companies',
        'app.companies_meta_description_translation',
        'app.companies_meta_title_translation',
        'app.companies_meta_keywords_translation',
        'app.slugs',
        'app.services',
        'app.companies_services',
        'app.companies_specializations',
        'app.members_specializations',
        'app.companies_members',
        'app.spoken_languages',
        'app.spoken_languages_title_translation',
        'app.members_spoken_languages'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('TopicComments') ? [] : ['className' => 'App\Model\Table\TopicCommentsTable'];
        $this->TopicComments = TableRegistry::get('TopicComments', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TopicComments);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
