<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ComparisonsReviewsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ComparisonsReviewsTable Test Case
 */
class ComparisonsReviewsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ComparisonsReviewsTable
     */
    public $ComparisonsReviews;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.comparisons_reviews',
        'app.comparisons',
        'app.comparisons_title_translation',
        'app.comparisons_content_translation',
        'app.comparisons_cta_translation',
        'app.comparisons_meta_description_translation',
        'app.comparisons_meta_title_translation',
        'app.comparisons_meta_keywords_translation',
        'app.comparisons_definition_title1_translation',
        'app.comparisons_definition_title2_translation',
        'app.comparisons_definition_content1_translation',
        'app.comparisons_definition_content2_translation',
        'app.comparisons_direction_title1_translation',
        'app.comparisons_direction_title2_translation',
        'app.comparisons_direction_title3_translation',
        'app.comparisons_direction_title4_translation',
        'app.comparisons_direction_content1_translation',
        'app.comparisons_direction_content2_translation',
        'app.comparisons_direction_content3_translation',
        'app.comparisons_direction_content4_translation',
        'app.i18n',
        'app.categories',
        'app.categories_title_translation',
        'app.categories_meta_description_translation',
        'app.categories_meta_title_translation',
        'app.categories_meta_keywords_translation',
        'app.categories_content_translation',
        'app.slugs',
        'app.languages',
        'app.translations',
        'app.brands',
        'app.brands_meta_description_translation',
        'app.brands_meta_title_translation',
        'app.brands_meta_keywords_translation',
        'app.brands_url_translation'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ComparisonsReviews') ? [] : ['className' => 'App\Model\Table\ComparisonsReviewsTable'];
        $this->ComparisonsReviews = TableRegistry::get('ComparisonsReviews', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ComparisonsReviews);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
