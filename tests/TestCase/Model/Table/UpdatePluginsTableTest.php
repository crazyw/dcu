<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UpdatePluginsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UpdatePluginsTable Test Case
 */
class UpdatePluginsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\UpdatePluginsTable
     */
    public $UpdatePlugins;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.update_plugins'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('UpdatePlugins') ? [] : ['className' => 'App\Model\Table\UpdatePluginsTable'];
        $this->UpdatePlugins = TableRegistry::get('UpdatePlugins', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UpdatePlugins);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
