<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TopicTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TopicTable Test Case
 */
class TopicTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TopicTable
     */
    public $Topic;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.topic',
        'app.members',
        'app.languages',
        'app.translations',
        'app.specializations',
        'app.specializations_title_translation',
        'app.i18n',
        'app.companies',
        'app.companies_meta_description_translation',
        'app.companies_meta_title_translation',
        'app.companies_meta_keywords_translation',
        'app.slugs',
        'app.services',
        'app.companies_services',
        'app.companies_specializations',
        'app.members_specializations',
        'app.companies_members',
        'app.spoken_languages',
        'app.spoken_languages_title_translation',
        'app.members_spoken_languages',
        'app.topic_categories',
        'app.topic_comments'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Topic') ? [] : ['className' => 'App\Model\Table\TopicTable'];
        $this->Topic = TableRegistry::get('Topic', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Topic);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
