<?php
namespace App\Test\TestCase\Controller\Admin;

use App\Controller\Admin\GenresController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\Admin\GenresController Test Case
 */
class GenresControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.genres',
        'app.movies',
        'app.directors',
        'app.producers',
        'app.companies',
        'app.slugs',
        'app.languages',
        'app.translations',
        'app.companies_meta_description_translation',
        'app.companies_meta_title_translation',
        'app.companies_meta_keywords_translation',
        'app.i18n',
        'app.specializations',
        'app.specializations_title_translation',
        'app.companies_specializations',
        'app.members',
        'app.members_specializations',
        'app.companies_members',
        'app.companies_movies',
        'app.genres_movies',
        'app.members_movies'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
